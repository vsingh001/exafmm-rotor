#ifndef postgpu_h
#define postgpu_h
//#include <cutil.h>
#include <helper_cuda.h>
#include <helper_math.h>

#define CALL_GPU(KERNEL,EVENT)\
void Kernel::KERNEL() {\
  cudaThreadSynchronize();\
  startTimer("cudaMalloc   ");\
  if( keysHost.size() > keysDevcSize ) {\
    if( keysDevcSize != 0 ) checkCudaErrors(cudaFree(keysDevc));\
    checkCudaErrors(cudaMalloc( (void**) &keysDevc,   keysHost.size()*sizeof(int) ));\
    keysDevcSize = keysHost.size();\
  }\
  if( rangeHost.size() > rangeDevcSize ) {\
    if( rangeDevcSize != 0 ) checkCudaErrors(cudaFree(rangeDevc));\
    checkCudaErrors(cudaMalloc( (void**) &rangeDevc,  rangeHost.size()*sizeof(int) ));\
    rangeDevcSize = rangeHost.size();\
  }\
  if( sourceHost.size() > sourceDevcSize ) {\
    if( sourceDevcSize != 0 ) checkCudaErrors(cudaFree(sourceDevc));\
    checkCudaErrors(cudaMalloc( (void**) &sourceDevc, sourceHost.size()*sizeof(gpureal) ));\
    sourceDevcSize = sourceHost.size();\
  }\
  if( targetHost.size() > targetDevcSize ) {\
    if( targetDevcSize != 0 ) checkCudaErrors(cudaFree(targetDevc));\
    checkCudaErrors(cudaMalloc( (void**) &targetDevc, targetHost.size()*sizeof(gpureal) ));\
    targetDevcSize = targetHost.size();\
  }\
  cudaThreadSynchronize();\
  stopTimer("cudaMalloc   ");\
  startTimer("cudaMemcpy   ");\
  checkCudaErrors(cudaMemcpy(keysDevc,  &keysHost[0],  keysHost.size()*sizeof(int),      cudaMemcpyHostToDevice));\
  checkCudaErrors(cudaMemcpy(rangeDevc, &rangeHost[0], rangeHost.size()*sizeof(int),     cudaMemcpyHostToDevice));\
  checkCudaErrors(cudaMemcpy(targetDevc,&targetHost[0],targetHost.size()*sizeof(gpureal),cudaMemcpyHostToDevice));\
  checkCudaErrors(cudaMemcpy(sourceDevc,&sourceHost[0],sourceHost.size()*sizeof(gpureal),cudaMemcpyHostToDevice));\
  checkCudaErrors(cudaMemcpyToSymbol(constDevc,&constHost[0],constHost.size()*sizeof(gpureal)));\
  cudaThreadSynchronize();\
  stopTimer("cudaMemcpy   ");\
  cudaThreadSynchronize();\
  startTimer(#EVENT);\
  int numBlocks = keysHost.size();\
  if( numBlocks != 0 ) {\
    KERNEL##_GPU<<< numBlocks, THREADS >>>(keysDevc,rangeDevc,targetDevc,sourceDevc);\
  }\
  getLastCudaError("Kernel execution failed");\
  cudaThreadSynchronize();\
  stopTimer(#EVENT);\
  cudaThreadSynchronize();\
  startTimer("cudaMemcpy   ");\
  checkCudaErrors(cudaMemcpy(&targetHost[0],targetDevc,targetHost.size()*sizeof(gpureal),cudaMemcpyDeviceToHost));\
  cudaThreadSynchronize();\
  stopTimer("cudaMemcpy   ");\
}

#endif
