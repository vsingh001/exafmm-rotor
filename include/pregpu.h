#ifndef pregpu_h
#define pregpu_h
#include <omp.h>

static size_t  keysDevcSize = 0;                                // Size of offsets for rangeHost
static size_t  rangeDevcSize = 0;                               // Size of offsets for sourceHost
static size_t  sourceDevcSize = 0;                              // Size of sources
static size_t  targetDevcSize = 0;                              // Size of targets
static int     *keysDevc;                                       // Keys on device
static int     *rangeDevc;                                      // Ranges on device
static gpureal *sourceDevc;                                     // Sources on device
static gpureal *targetDevc;                                     // Targets on device
#pragma omp threadprivate(keysDevcSize,rangeDevcSize,sourceDevcSize,targetDevcSize)
#pragma omp threadprivate(keysDevc,rangeDevc,sourceDevc,targetDevc)
__device__ __constant__ gpureal constDevc[1];                   // Constants on device

namespace {
__device__ void cart2sph(gpureal& r, gpureal& theta, gpureal& phi, gpureal dx, gpureal dy, gpureal dz) {
  r = sqrtf(dx * dx + dy * dy + dz * dz)+EPS;
  theta = acosf(dz / r);
  if( fabs(dx) + fabs(dy) < EPS ) {
    phi = 0;
  } else if( fabs(dx) < EPS ) {
    phi = dy / fabs(dy) * M_PI * 0.5;
  } else if( dx > 0 ) {
    phi = atanf(dy / dx);
  } else {
    phi = atanf(dy / dx) + M_PI;
  }
}

__device__ void sph2cart(gpureal r, gpureal theta, gpureal phi, gpureal *spherical, gpureal *cartesian) {
  cartesian[0] = sinf(theta) * cosf(phi) * spherical[0]
               + cosf(theta) * cosf(phi) / r * spherical[1]
               - sinf(phi) / r / sinf(theta) * spherical[2];
  cartesian[1] = sinf(theta) * sinf(phi) * spherical[0]
               + cosf(theta) * sinf(phi) / r * spherical[1]
               + cosf(phi) / r / sinf(theta) * spherical[2];
  cartesian[2] = cosf(theta) * spherical[0]
               - sinf(theta) / r * spherical[1];
}

__device__ void evalMultipole(gpureal *YnmShrd, gpureal rho, gpureal alpha, gpureal *factShrd) {
  gpureal x = cosf(alpha);
  gpureal s = sqrtf(1 - x * x);
  gpureal fact = 1;
  gpureal pn = 1;
  gpureal rhom = 1;
  for( int m=0; m<P; ++m ){
    gpureal p = pn;
    int npn = m * m + 2 * m;
    int nmn = m * m;
    YnmShrd[npn] = rhom * p / factShrd[2*m];
    YnmShrd[nmn] = YnmShrd[npn];
    gpureal p1 = p;
    p = x * (2 * m + 1) * p;
    rhom *= -rho;
    gpureal rhon = rhom;
    for( int n=m+1; n<P; ++n ){
      int npm = n * n + n + m;
      int nmm = n * n + n - m;
      YnmShrd[npm] = rhon * p / factShrd[n+m];
      YnmShrd[nmm] = YnmShrd[npm];
      gpureal p2 = p1;
      p1 = p;
      p = (x * (2 * n + 1) * p1 - (n + m) * p2) / (n - m + 1);
      rhon *= -rho;
    }
    pn = -pn * fact * s;
    fact += 2;
  }
}

__device__ void evalLocal(gpureal *YnmShrd, gpureal rho, gpureal alpha, gpureal *factShrd) {
  gpureal x = cosf(alpha);
  gpureal s = sqrtf(1 - x * x);
  gpureal rho_1 = 1 / rho;
  for( int l=threadIdx.x; l<(2*P+1)*P; l+=THREADS ){
    gpureal fact = 1;
    gpureal pn = 1;
    gpureal rhom = rho_1;
    int nn = floor(sqrtf(2*l+0.25)-0.5);
    int mm = 0;
    gpureal Ynm;
    for( int i=0; i<=nn; ++i ) mm += i;
    mm = l - mm;
    int n;
    for( int m=0; m<mm; ++m ){
      rhom *= rho_1;
      pn = -pn * fact * s;
      fact += 2;
    }
    int m = mm;
    gpureal p = pn;
    if( mm == nn ) Ynm = rhom * p;
    gpureal p1 = p;
    p = x * (2 * m + 1) * p;
    rhom *= rho_1;
    gpureal rhon = rhom;
    for( n=m+1; n<nn; ++n ){
      gpureal p2 = p1;
      p1 = p;
      p = (x * (2 * n + 1) * p1 - (n + m) * p2) / (n - m + 1);
      rhon *= rho_1;
    }
    if( n <= nn ) Ynm = rhon * p * factShrd[n-m];
    YnmShrd[l] = Ynm;
  }
  __syncthreads();
}
}

#endif
