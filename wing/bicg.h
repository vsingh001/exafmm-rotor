
/**
Multiply matrix with vector for a square matrix
*/
void matVecMult(std::vector<double> &A, std::vector<double> &x, std::vector<double> &prod)
{
	int columns = x.size();	
	for(int i=0;i<columns;i++)
	{
		prod[i] = 0.0;
		for(int j=0;j<columns;j++)
		{
			int index = i*columns+j;
			prod[i]+=A[index]*x[j];
		}
	}
}


/**
Dot product of vectors
*/
double dot(std::vector<double> &a, std::vector<double> &b)
{
	double dotProduct = 0;
	for(int i=0;i<b.size();i++)
	{
		dotProduct+=a[i]*b[i];	
	}
	return dotProduct;
}



/**
Use BiCG Stab method to solve Ax=b
*/
void solveBICG(std::vector<double> &A, std::vector<double> &b, int &columns, std::vector<double> &x)
{

	double eps = std::numeric_limits<double>::epsilon();

	std::vector<double> r(columns), nu(columns), p(columns), rHat(columns); 	
	double rho, alpha, omega;
	for(int i=0;i<columns;i++)
	{
		x[i] = 0; nu[i] = 0; p[i] = 0;
		r[i] = b[i];
		rHat[i] = r[i]; 
//		std::cout << rHat[i] << std::endl;
	}
	rho = 1; alpha = 1;omega = 1;
	for(int j=0;j<50;j++)
	{
		double tempRho = dot(rHat, r);
		double beta = (tempRho/rho)*(alpha/omega);
//		std::cout << j << "\t" << tempRho << "\t" << alpha << "\t" << omega<< "\t" << beta << std::endl;
		rho = tempRho;
		for(int i=0;i<columns;i++)
		{
			p[i] = r[i]+beta*(p[i]-omega*nu[i]);
		}
		matVecMult(A, p, nu);		
//		for(int i=0;i<columns;i++)
//			std::cout << p[i] << "\t" << nu[i] << std::endl;

		alpha = rho/dot(rHat, nu);
//		std::cout << rho << "\t" << dot(rHat, nu) <<  std::endl;
		std::vector<double> s(columns);
//		std::cout << tempRho << "\t" << alpha << std::endl;
		for(int i=0;i<columns;i++)
		{
			s[i] = r[i]-alpha*nu[i];
//			std::cout << p[i] << "\t" <<  s[i] << std::endl;
		}
		std::vector<double> t(columns);
		matVecMult(A, s, t);
		omega = dot(t,s)/dot(t,t);
//		for(int i=0;i<columns;i++)
//		{
//			std::cout << s[i] << "\t" << t[i] << std::endl;
//		}
		
		for(int i=0;i<columns;i++)
		{
			x[i] += alpha*p[i]+omega*s[i];
			r[i] = s[i] - omega*t[i];
//			std::cout << s[i] << "\t" << x[i] << "\t" <<  std::endl;
		}
//		std::cout << rho << "\t" << alpha << "\t" << omega << "\t" << beta << std::endl;
		double residual = dot(r, r);
		if (residual < 1e-9)
			break;
//		std::cout << residual << std::endl;
	}	
//	matVecMult(A, x, p);
//	for(int i=0;i<columns;i++)
//		std::cout << b[i] << "\t" << p[i] << "\t" << x[i]<< std::endl;	
}









/**
For purely testing reasons created a copy of bicg
So that when I print I print onyl for those cases that I need to test with
*/
void solveBICG1(std::vector<double> &A, std::vector<double> &b, int &columns, std::vector<double> &x)
{

	double eps = std::numeric_limits<double>::epsilon();

	std::vector<double> r(columns), nu(columns), p(columns), rHat(columns); 	
	double rho, alpha, omega;
	for(int i=0;i<columns;i++)
	{
		x[i] = 0; nu[i] = 0; p[i] = 0;
		r[i] = b[i];
		rHat[i] = r[i]; 
//		std::cout << rHat[i] << std::endl;
	}
	rho = 1; alpha = 1;omega = 1;
	for(int j=0;j<50;j++)
	{
		double tempRho = dot(rHat, r);
		double beta = (tempRho/rho)*(alpha/omega);
//		std::cout << j << "\t" << tempRho << "\t" << alpha << "\t" << omega<< "\t" << beta << std::endl;
		rho = tempRho;
		for(int i=0;i<columns;i++)
		{
			p[i] = r[i]+beta*(p[i]-omega*nu[i]);
		}
		matVecMult(A, p, nu);		
//		for(int i=0;i<columns;i++)
//			std::cout << p[i] << "\t" << nu[i] << std::endl;

		alpha = rho/dot(rHat, nu);
//		std::cout << rho << "\t" << dot(rHat, nu) <<  std::endl;
		std::vector<double> s(columns);
//		std::cout << tempRho << "\t" << alpha << std::endl;
		for(int i=0;i<columns;i++)
		{
			s[i] = r[i]-alpha*nu[i];
//			std::cout << p[i] << "\t" <<  s[i] << std::endl;
		}
		std::vector<double> t(columns);
		matVecMult(A, s, t);
		omega = dot(t,s)/dot(t,t);
//		for(int i=0;i<columns;i++)
//		{
//			std::cout << s[i] << "\t" << t[i] << std::endl;
//		}
		
		for(int i=0;i<columns;i++)
		{
			x[i] += alpha*p[i]+omega*s[i];
			r[i] = s[i] - omega*t[i];
//			std::cout << s[i] << "\t" << x[i] << "\t" <<  std::endl;
		}
//		std::cout << rho << "\t" << alpha << "\t" << omega << "\t" << beta << std::endl;
		double residual = dot(r, r);
		if (residual < 1e-9)
			break;
//		std::cout << residual << std::endl;
	}	
	matVecMult(A, x, p);
	for(int i=0;i<columns;i++)
		std::cout << b[i] << "\t" << p[i] << "\t" << x[i]<< std::endl;	
}







