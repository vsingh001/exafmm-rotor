#ifndef body_h
#define body_h
#include <assert.h>
#include <cmath>
#include <complex>
#include <cstdlib>
#include <list>
#include <iostream>
#include <stack>
#include <sys/time.h>
#include <vector>
#include <vec.h>

typedef long                 bigint;                            // Big integer type
typedef float                real;                              // Real number type on CPU
typedef float                gpureal;                           // Real number type on GPU
typedef std::complex<double> complex;                           // Complex number type

const int P = 3;
const int NCRIT = 8;
const real THETA = 0.6;
const real EPS2 = 1e-4;

const int MCOEF = P*(P+1)*(P+2)/6-3;
const int LCOEF = (P+1)*(P+2)*(P+3)/6;
const int NCOEF = P*(P+1)*(P+2)/6;
typedef vec<3 ,real> vect;
#if CART
typedef vec<NCOEF,real> Mset;
typedef vec<NCOEF,real> Lset;
#elif SPHE
typedef vec<NCOEF,complex> Mset;
typedef vec<NCOEF,complex> Lset;
#else
typedef vec<MCOEF,real> Mset;
typedef vec<LCOEF,real> Lset;
#endif
const real zero = 0.;

double get_time(void) {
  struct timeval tv;
  struct timezone tz;
  gettimeofday(&tv, &tz);
  return ((double)(tv.tv_sec+tv.tv_usec*1.0e-6));
}

struct JBody {                                                  // Source properties of a body (stuff to send)
  int         IBODY;                                            // Initial body numbering for sorting back
//  int         IPROC;                                            // Initial process numbering for partitioning back
//  bigint      ICELL;                                            // Cell index
  vect        X;                                                // Position
  vec<1,real> SRC;                                              // Source values
};
struct Body : JBody {                                           // All properties of a body
  vec<4,real> TRG;                                              // Target values
  bool operator<(const Body &rhs) const {                       // Overload operator for comparing body index
    return this->IBODY < rhs.IBODY;                             // Comparison function for body index
  }
};
typedef std::vector<Body>           Bodies;                     // Vector of bodies
typedef std::vector<Body>::iterator B_iter;                     // Iterator for body vector

struct Leaf {
  int I;
  vect X;
  Leaf *NEXT;
};
typedef std::vector<Leaf>           Leafs;                      // Vector of leafs
typedef std::vector<Leaf>::iterator L_iter;                     // Iterator for leaf vector

struct Node {
  int  LEVEL;
  int  ICHILD;
  int  NLEAF;
  int  CHILD[8];
  vect X;
  Leaf *LEAF;
};
typedef std::vector<Node>           Nodes;
typedef std::vector<Node>::iterator N_iter;

struct Cell {
  unsigned NCHILD;
  unsigned NCLEAF;
  unsigned NDLEAF;
  unsigned PARENT;
  unsigned CHILD;
  B_iter   LEAF;
  vect X;
  real R;
  real RCRIT;
  Mset M;
  Lset L;
};
typedef std::vector<Cell>           Cells;
typedef std::vector<Cell>::iterator C_iter;
#endif
