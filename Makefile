.SUFFIXES: .cxx .cu .o

DEVICE  = cpu
#DEVICE  = gpu

#EXPAND  = Cartesian
EXPAND  = Spherical

SDK_INSTALL_PATH=/usr/local/cuda-7.5/samples/
CUDA_INSTALL_PATH=/usr/local/cuda-7.5/

#CXX     = mpicxx -mpreferred-stack-boundary=4 -ggdb3 -Wall -Wextra -Winit-self -Wshadow -O3 -fPIC -fopenmp\
	-ffast-math -funroll-loops -fforce-addr -rdynamic -D_FILE_OFFSET_BITS=64\
	-I. -I../include -I/usr/local/fftw/include -I/usr/include/vtk-5.2 -L/usr/lib/vtk-5.2
#CXX     = mpicxx -O2 -fPIC -fopenmp -I../include -I/usr/local/fftw/include -I/usr/include/vtk-5.2 -L/usr/lib/vtk-5.2
CXX     = mpicxx -g -I../include -I/home/vikram/Downloads/Installs/src/petsc-3.6.2/include -I/home/vikram/Downloads/Installs/src/petsc-3.6.2/arch-linux2-c-debug/include/ -static-libstdc++ -std=c++0x
#CXX     = mpicxx -mpreferred-stack-boundary=4 -ggdb3 -Wall -Wextra -Winit-self -Wshadow -O3 -fPIC -fopenmp\
	-ffast-math -funroll-loops -fforce-addr -rdynamic -D_FILE_OFFSET_BITS=64\
	 -I/home/vikram/Downloads/Installs/src/petsc-3.6.2/include -I/home/vikram/Downloads/Installs/src/petsc-3.6.2/arch-linux2-c-debug/include/ \
	-I. -I../include -std=c++0x

NVCC    = nvcc  -m64    -Xcompiler -fopenmp -gencode arch=compute_30,code=sm_30 \
	-gencode arch=compute_35,code=sm_35 -gencode arch=compute_37,code=sm_37 \
	-gencode arch=compute_50,code=sm_50 -gencode arch=compute_52,code=sm_52 \
	-gencode arch=compute_52,code=compute_52 \
	 --ptxas-options=-v -O3 -use_fast_math \
	-I../include -I$(CUDA_INSTALL_PATH)/include -I$(SDK_INSTALL_PATH)/common/inc \

#NVCC = nvcc -g \
	-I../include -I$(CUDA_INSTALL_PATH)/include -I$(SDK_INSTALL_PATH)/common/inc \
	
LFLAGS  = -L$(CUDA_INSTALL_PATH)/lib64 -L$(SDK_INSTALL_PATH)/lib \
	-lcuda -lcudart -lstdc++ -ldl -lm  \
	-D$(DEVICE) -D$(EXPAND)
#VFLAGS  = -lvtkHybridTCL -lvtkWidgetsTCL -DVTK
#OBJECT  = ../kernel/$(DEVICE)$(EXPAND)Laplace.o ../kernel/$(DEVICE)$(EXPAND)BiotSavart.o\
#	../kernel/$(DEVICE)$(EXPAND)Stretching.o ../kernel/$(DEVICE)$(EXPAND)Gaussian.o\
#	../kernel/$(DEVICE)$(EXPAND)CoulombVdW.o

OBJECT  = ../kernel/$(DEVICE)$(EXPAND)BiotSavart.o ../kernel/$(DEVICE)$(EXPAND)Gaussian.o\
	../kernel/$(DEVICE)$(EXPAND)Stretching.o ../kernel/$(DEVICE)$(EXPAND)Laplace.o\
	../kernel/$(DEVICE)$(EXPAND)CoulombVdW.o

.cxx.o  :
	$(CXX) -c $? -o $@ $(LFLAGS)
.cu.o   :
	$(NVCC) -c $? -o $@ $(LFLAGS)
cleanall:
	rm -f ../unit_test/*.o ../unit_test/*.out ../unit_test/*.dat ../unit_test/*.sum ../unit_test/direct0*
	rm -f ../example/*.o ../example/*.out ../example/time ../kernel/*.o ../wrapper/*.o
	rm -f ../wrapper/*.o ../wrapper/*.out ../wrapper/*.a ../wrapper/*.dat
	rm -f ../vortex/*.o ../vortex/*.out ../vortex/*.dat
	rm -f ../fast/*.o ../fast/*.out ../fast/*.dat
save    :
	make cleanall
	tar zcvf ../../exafmm.tgz ../../exafmm
