double getEps()
{
	return std::numeric_limits<double>::epsilon();
}

struct ordering {
    bool operator ()(Body const& a, Body const& b) {
        return a.SRC[3] < b.SRC[3];
    }
};

/*
Create blade segments according to Zhao et al '09's equal annulus rule
*/
void creatBladSegm(std::vector<double> &r, const int segments)
{
	double *radius = new double[segments];

	radius[0]=1;
	radius[1]=sqrt(2);
	for(int i=2;i<segments;i++)
	{
		radius[i]=sqrt(2*radius[i-1]*radius[i-1]-radius[i-2]*radius[i-2]);
	}

	for(int i=0;i<segments;i++)
		radius[i]=radius[i]/radius[segments-1];

	r[0] = 0.5*radius[0];
	for(int i=1;i<segments;i++)
		r[i] = 0.5*(radius[i]+radius[i-1]);

	delete radius;
}

/**
Get area of segment. Since it is by equal annulus rule, it will be just one value
*/
void getNonDimAnnulusR0(double &area, const int segments)
{
	std::vector<double> r(segments);
	creatBladSegm(r, segments);
}


void initSource(Bodies &bodies)
{
	for( B_iter B=bodies.begin(); B!=bodies.end(); ++B )
	{
		B->IBODY = B-bodies.begin();
		B->SRC = 0;
		B->SRC[2] = 0.0 ;

		B->SRC[3] = 0.2;  //sigma (core size)

		B->X[0] = 0.0;
		B->X[1] = 0.0;
		B->X[2] = 0.0;
	}
}


void initTarget(Bodies &bodies, std::vector<double> x, std::vector<double> y)
{
	int numTarget = bodies.end() - bodies.begin();
	for( B_iter B=bodies.begin(); B!=bodies.end(); ++B )
	{
		B->IBODY = B-bodies.begin();
		B->IPROC = MPIRANK;

		int i = B-bodies.begin();

		B->TRG = 0;
		B->TRG[0] = 0;
		B->TRG[1] = 0;
		B->TRG[2] = 0;

		B->X[0] = x[i];
		B->X[1] = y[i];
		B->X[2] = 0.0;

//		std::cout << B->X[0] << "\t" << B->X[1] << std::endl;
	}
}


/*
Will give blade points at current time assuming it starts at theta = 0 at time = 0
Input -> dimensions, time
*/
void getBladePoints(std::vector<double> &x, std::vector<double> &y, double time, double rpm, double chord, double span, const int segments)
{
	std::vector<double> r(segments);
	creatBladSegm(r, segments); //Get co-ordinates of mid-point of blade in r direction

	double radPerSec = rpm*2*M_PI/(60.0);
	double theta = radPerSec*time;

	for(int i=0;i<segments;i++)
	{
		r[i] = r[i]*span;
		x[i] = r[i]*cos(theta);
		y[i] = r[i]*sin(theta);

//		std::cout << x[i] << "\t" << y[i] << "\t" << std::endl;
	}
}


void getBladeVel(std::vector<double> &solU, std::vector<double> &solV, std::vector<double> &solW, std::vector<double> x, std::vector<double> y, double time, double rpm, int segments)
{
	double radPerSec = rpm*2*M_PI/(60.0);
	double theta = radPerSec*time;

	std::vector<double> r(segments);

	for(int i=0;i<segments;i++)
	{
		r[i] = sqrt(pow(x[i],2)+pow(y[i],2));
		double rNormVel = r[i]*radPerSec;
		solU[i] = -rNormVel*sin(theta);
		solV[i] = rNormVel*cos(theta);
		solW[i] = 0;
	}
}


/*
Get bound circulation of a segment
*/
double getBoundCirculationNACA0015(double alpha, double vel, double chord)
{
	std::ifstream readFile;

	readFile.open("naca0015.txt");

	std::vector<double> alp, cl;

	double temp1, temp2, temp;
	while(!readFile.eof())
	{
		readFile >> temp1 >> temp2 >> temp >> temp >> temp >> temp >> temp;
		alp.push_back(temp1);
		cl.push_back(temp2);
	}

	alpha = alpha*180.0/M_PI;
	double secCl; //Cl of segment
	for(int i=0;i<alp.size()-1;i++)
	{
		if ((alpha >= alp[i]) && (alpha < alp[i+1]))
		{
//			std::cout << alpha << "\t" << alp[i] << std::endl;
			secCl = cl[i] + (alpha-alp[i])*(cl[i+1]-cl[i])/(alp[i+1]-alp[i]);
		}
	}
	readFile.close();

	double boundCir = 0.5*vel*chord*secCl;//0.5*rho*v*v*c*Cl = rho*v*Gamma
	return boundCir;
}

/*
Get bound circulation of a segment
*/
double getLiftCurveSlopeNACA0015()
{
	std::ifstream readFile;

	readFile.open("naca0015.txt");

	std::vector<double> alp, cl;

	double temp1, temp2, temp;
	while(!readFile.eof())
	{
		readFile >> temp1 >> temp2 >> temp >> temp >> temp >> temp >> temp;
		alp.push_back(temp1);
		cl.push_back(temp2);
	}

	double alpha = 0*180.0/M_PI; //Get slope at alpha=0
	double clAlpha; //Lift Curve Slope segment
	for(int i=0;i<alp.size()-1;i++) 
	{
		if ((alpha >= alp[i]) && (alpha < alp[i+1]))
		{
//			std::cout << alp[i] << "\t" << alp[i+1] << "\t" << cl[i] << "\t" << cl[i+1]<< std::endl;
			clAlpha = (cl[i+1]-cl[i])/(alp[i+1]-alp[i]);
		}
	}
	readFile.close();
	
	return clAlpha*180.0/M_PI; //Scale for radians
}


void getBoundCirculation(std::vector<double>  &boundCir, std::vector<double> alpha, std::vector<double> vel, std::vector<double> &x, std::vector<double> &y, const double &chord, const double &span)
{
//	for(int i=0;i<alpha.size();i++)
//	{
//		boundCir[i] = getBoundCirculationNACA0015(alpha[i], vel[i], chord);
//		std::cout << alpha[i] << "\t" << vel[i] << "\t" << boundCir[i] << std::endl;
//	}

//	double clAlpha = getLiftCurveSlopeNACA0015();
	double clAlpha = 0.2*180/M_PI;

	int segments = boundCir.size();

	std::vector<double> Acirc((segments-2)*(segments-2));
	std::vector<double> bCirc(segments-2);
	std::vector<double> solCirc(segments-2);

	std::vector<double> rSeg(segments);//Radial location of segments
	for(int i=0;i<segments;i++)
		rSeg[i] = sqrt(x[i]*x[i]+y[i]*y[i]);
	for(int i=0;i<segments-2;i++)
	{
		
		for(int j=0;j<segments-2;j++)
		{
			/**
			Use discrete vortex modeling approach to solve for vorticity
			*/
			int index = i*(segments-2)+j;
			double coeff1 = 0.5*clAlpha*chord*0.25*M_PI*(1/(rSeg[i+1]-0.5*(rSeg[j]+rSeg[j+1])));
			double coeff2 = -0.5*clAlpha*chord*0.25*M_PI*(1/(rSeg[i+1]-0.5*(rSeg[j+1]+rSeg[j+2])));
			Acirc[index] = coeff1+coeff2;
//			std::cout << Acirc[index] << "\t" ;
		}
//		std::cout << std::endl;
		int index = i*(segments-2)+i;
		Acirc[index] += 1;
		bCirc[i] = 0.5*vel[i+1]*alpha[i+1]*clAlpha*chord;
	}
	int columns = segments-2;

	solveBICG(Acirc, bCirc, columns, solCirc);

	for(int i=0;i<segments-2;i++)
		boundCir[i+1] = solCirc[i];

	boundCir[0] = 0.0;//Impose boundary condition at root and tip explicitly
	boundCir[segments-1] = 0.0;

//	std::ofstream wrFile;
//	std::string name="circ";
//	wrFile.open(name);
//	for(int i=0;i<segments;i++)
//		wrFile << rSeg[i] << "\t" << boundCir[i] << std::endl;	

//	wrFile.close();

}


double getThreePtDeri(double rMinus, double r, double rPlus, double fnMinus, double fn, double fnPlus)
{
	double eps = getEps();

	double delRPlusHalf = rPlus-r;
	double delRMinusHalf = r-rMinus;
	//Coefficients for 3 point derivative
	double aMin = -delRPlusHalf/(delRMinusHalf*(delRPlusHalf+delRMinusHalf)+eps);
	double a0 = (delRPlusHalf-delRMinusHalf)/((delRPlusHalf*delRMinusHalf)+eps);
	double aPlus = delRMinusHalf/(delRPlusHalf*(delRPlusHalf+delRMinusHalf)+eps);

	return aMin*fnMinus+a0*fn+aPlus*fnPlus;
}


//Do the derivative computation to get vorticity source from bound circulation
void getVorticitySource(std::vector<double> &shedVort, std::vector<double> &trailVort, std::vector<double>  &x, std::vector<double>  &y, std::vector<double> &circT0, std::vector<double> &circT1, double dt)
{
	int segments = circT0.size();

	for(int i=0;i<segments;i++)
	{
		shedVort[i] = -1*(circT1[i]-circT0[i])/dt;//-dGamma/dt

		double r = sqrt(pow(x[i],2)+pow(y[i],2));
		if((i>0) && (i<segments-1))
		{
			double rPlus = sqrt(pow(x[i+1],2)+pow(y[i+1],2));
			double rMinus  = sqrt(pow(x[i-1],2)+pow(y[i-1],2));

//Test the derivative
//			std::cout << i << "\t" << getThreePtDeri(i-1, i, i+1, pow(i-1,2), pow(i,2), pow(i+1,2)) << std::endl;

			trailVort[i] = getThreePtDeri(rMinus, r, rPlus, circT1[i-1], circT1[i], circT1[i+1]);
//			std::cout << rMinus << "\t" << trailVort[i] << "\t" << rPlus << std::endl;
//			std::cout << rMinus << "\t" << circT1[i-1] << "\t" << r << "\t" << circT1[i] << std::endl;
		}
		else if (i==0)
		{
			double rPlus = sqrt(pow(x[i+1],2)+pow(y[i+1],2));
			trailVort[i] =  (circT1[i+1]-circT1[i])/(rPlus-r);
		}
		else if (i==(segments-1))
		{
			double rMinus  = sqrt(pow(x[i-1],2)+pow(y[i-1],2));
			trailVort[i] =  (circT1[i]-circT1[i-1])/(r-rMinus);//divergence of gamma
		}
	}
}



void getSourceVector(std::vector<double> &sourceX, std::vector<double> &sourceY, std::vector<double> &sourceZ, std::vector<double> &shedVort, std::vector<double> &trailVort, std::vector<double> &u, std::vector<double> &v, std::vector<double> &w, double theta)
{
	int segments = shedVort.size();

	for(int i=0;i<segments;i++)
	{
		sourceX[i] = shedVort[i]*cos(theta)-u[i]*trailVort[i];//Vb is minus of v//
		sourceY[i] = shedVort[i]*sin(theta)-v[i]*trailVort[i];//Vb is minus of v
		sourceZ[i] = 			   -w[i]*trailVort[i];//Vb is minus of v

//		std::cout << sourceX[i] << "\t" << sourceY[i] << "\t" << sourceZ[i] << std::endl;
	}
}



/*
Assume that at the present time the blade got here from theta-dt*omega
*/
void getSegmentMid(std::vector<double> &ringMidX, std::vector<double> &ringMidY, std::vector<double> &x, std::vector<double> &y, double rpm, double t, double dt)
{

	int segments = x.size();

	double dThetaHalf = rpm*2*M_PI/60.0;
	double theta = t*rpm*2*M_PI/60.0;
	double ringMidTheta = theta-dThetaHalf;
	for(int i=0;i<segments;i++)
	{
		double r=sqrt(x[i]*x[i]+y[i]*y[i]);
		ringMidX[i] = r*cos(ringMidTheta);
		ringMidY[i] = r*sin(ringMidTheta);
	}

}


void createGrid(std::vector<double> &inX, std::vector<double> &inY, std::vector<double> &outX, std::vector<double> &outY, const double &span)
{
		int segments = outX.size();
		double theta = atan2(inY[segments-1], inX[segments-1]);

		for(int i=0;i<segments;i++)
		{
			outX[i] = (i+1)*span*(cos(theta))/segments;
			outY[i] = (i+1)*span*(sin(theta))/segments;
		//	std::cout << outX[i] << "\t"<< outY[i] << std::endl;
		}
}


/**Create a new grid, take sources and use gaussian+bicg to determine source values
Pass it to accumSource to add as sources
llX -> lifting line X
lvX -> lifting line vorticity gamma
alphaX -> vorticity*volume
*/
void reMesh(std::vector<double> &llX, std::vector<double> &llY, std::vector<double> &lvX, std::vector<double> &lvY, std::vector<double> &lvZ, double &sigma, const double &span, std::vector<double> &cellX, std::vector<double> &cellY, std::vector<double> &alphX, std::vector<double> &alphY, std::vector<double> &alphZ)
{
	TreeConstructor T;
	Dataset D;
	Bodies bodies;

	int segments = llX.size();
	bodies.resize(segments);

	createGrid(llX, llY, cellX, cellY, span);

}


