/*
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include "dataset.h"
#include "construct.h"
#ifdef VTK
#include "vtk.h"
#endif
#include <math.h>
#include <fstream>
#include <limits>


#include "bicg.h"


double getEps()
{
	return std::numeric_limits<double>::epsilon();
}

struct ordering {
    bool operator ()(Body const& a, Body const& b) {
        return a.SRC[3] < b.SRC[3];
    }
};

//Update according to Vortex Transport Equation
void update(Bodies &bodies, std::vector<double> &dxdt, std::vector<double> &dydt, std::vector<double> &dzdt, double dt, double nu)
{
	for(B_iter B=bodies.begin();B!=bodies.end();++B)
	{
		int i = B-bodies.begin();		

		B->X[0] += dxdt[i] * dt; //Update positions from Biot-Savart velocities
		B->X[1] += dydt[i] * dt;
		B->X[2] += dzdt[i] * dt;

		B->SRC[0] += B->TRG[0] * dt; //Source values change according to stretching
		B->SRC[1] += B->TRG[1] * dt;
		B->SRC[2] += B->TRG[2] * dt;

		B->SRC[3] += (nu / B->SRC[3]) * dt; //Diffusion 

//		std::cout <<  dxdt[i] << "\t" << dydt[i] << "\t" << dzdt[i] << "\t "<< B->X << std::endl << std::endl;

	}
}


int main() {
  IMAGES = 0;
  THETA = 1 / sqrtf(4);
  Bodies bodies, jbodies, kbodies;
  Cells cells, jcells;

	std::string kernelName = "BiotSavart";

	Dataset D;
	TreeConstructor T;

	T.setKernel(kernelName);
	T.initialize();

	D.kernelName = kernelName;
//Major FMM parameters are in types.h

	const double dt = 1e-4   ;
	const double nu = 1.5e-5;
	/*
	Define blade characteristics
	*/
	const double rpm = 960; const double chord = 2*0.0254; const double span = 30*0.0254;
	double alpha = 4*M_PI/180.0;
	const int segments = 25;
	std::vector<double> x(segments), y(segments);

	double timePerRev = 60.0/rpm;
	int stepsForOneRev = timePerRev/dt;

	double h = 0.025*span;//Resolution of vorticity (Seems arbitrary)
	double sigma = 1.3*h;//Vorticity Core size from Zhao '09

	int numBodies = 2000;
	int numTarget = segments;
	bodies.resize(numBodies);

	for(B_iter B=bodies.begin(); B!=bodies.end(); ++B)
	{
		int i = B-bodies.begin();
		
		double vorticity = 400;
		if (i<1000)
		{
			double theta = i*4*M_PI/numBodies;
			B->X[0] = span*cos(theta);
			B->X[1] = span*sin(theta);
			B->X[2] = 0;
			B->SRC[0] = vorticity*h*h*h*sin(theta);
			B->SRC[1] = -vorticity*h*h*h*cos(theta);
		}
		else
		{
			double theta = (i-1000)*4*M_PI/numBodies;
			B->X[0] = (span-0.05*span)*cos(theta);
			B->X[1] = (span-0.05*span)*sin(theta);
			B->X[2] = 0;
			B->SRC[0] = (vorticity-0.05*vorticity)*h*h*h*sin(theta);
			B->SRC[1] = -(vorticity-0.05*vorticity)*h*h*h*cos(theta);
		}

		B->SRC[2] = 0*h*h*h;

		B->SRC[3] = sigma; 

//		std::cout <<  B->X << "\t" << B->SRC << std::endl;

	}




  for(int timeIt=0;timeIt<5000;timeIt++)
  {
	double t = timeIt*dt;

	std::cout << "Ring case 1 Physical time elapsed\t" << t << std::endl;

	T.setDomain(bodies);
	cells.clear(); //NEVER FORGET this step. Creates all sorts of errors
	T.bottomup(bodies,cells);
	jcells = cells;
	T.downward(cells,jcells,1);

	/*
	1. BiotSavart
	2. Stretching
	3. update

	Bottomup is done only once. Bottomup includes P2M and M2M. So for each kernel I have to do it again.
	*/
	
	//BiotSavart
	T.setKernel("BiotSavart");
	D.initTarget(bodies);//NEVER Forget this step. The values are incremented. So if the base value is wrong, so is the final value
	T.evalP2M(cells);//So everytime bottomup is done, this is evaluated
	T.evalM2M(cells);
	jcells = cells;
	T.downward(cells,jcells,1);


	int numSourc = bodies.size();
	std::vector<double> dxdt(numSourc), dydt(numSourc), dzdt(numSourc);

	for(B_iter B=bodies.begin();B!=bodies.end();++B)
	{
		int i=B-bodies.begin();
		dxdt[i] = B->TRG[0];
		dydt[i] = B->TRG[1];
		dzdt[i] = B->TRG[2];
	}


	//Stretching
	T.setKernel("Stretching");
	D.initTarget(bodies);
	T.evalP2M(cells);
	T.evalM2M(cells);
	jcells = cells;
	T.downward(cells,jcells,1);

	//Update source and position
	update(bodies, dxdt, dydt, dzdt, dt, nu);



	if (((timeIt%(stepsForOneRev/10)) == 0) && (timeIt > 1))
	{
		std::ofstream wrFile;
		int noOfRev = timeIt/(stepsForOneRev/10);
		std::string name="ring1Particles"+std::to_string(noOfRev)+".vtk";		
		wrFile.open(name);
		wrFile << "# vtk DataFile Version 2.0 \nVortex Data \nASCII" << std::endl << "DATASET UNSTRUCTURED_GRID" << std::endl;
		wrFile << "POINTS\t" << bodies.size() << "\tfloat" << std::endl;
		for(B_iter B=bodies.begin();B!=bodies.end();++B)
		{
			wrFile << B->X[0] << "\t" << B->X[1] << "\t" << B->X[2] <<  std::endl;
		}
		wrFile << "CELLS\t" << bodies.size() << "\t" << 2*bodies.size() << std::endl;
		for(B_iter B=bodies.begin();B!=bodies.end();++B)
		{
			int i = B-bodies.begin();
			wrFile << "1\t" << i <<  std::endl;
		}
		wrFile << "CELL_TYPES\t" << bodies.size() << std::endl;
		for(B_iter B=bodies.begin();B!=bodies.end();++B)
		{
			int i = B-bodies.begin();
			wrFile << "1\t" <<  std::endl;
		}

		wrFile << "POINT_DATA\t" << bodies.size() << std::endl;
		wrFile << "VECTORS \t tot_vorticity \t float" << std::endl;
		for(B_iter B=bodies.begin();B!=bodies.end();++B)
		{
			wrFile << B->SRC[0]/(h*h*h) << "\t" << B->SRC[1]/(h*h*h) << "\t" << B->SRC[2]/(h*h*h) << "\t" <<  std::endl;
		}

		wrFile.close();	
	}//If loop

	}//For loop


	T.finalize();


}
