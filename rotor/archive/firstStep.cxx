/*
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include "dataset.h"
#include "construct.h"
#ifdef VTK
#include "vtk.h"
#endif
#include <math.h>
#include <fstream>
#include <limits>


#include "bicg.h"


double getEps()
{
	return std::numeric_limits<double>::epsilon();
}

struct ordering {
    bool operator ()(Body const& a, Body const& b) {
        return a.SRC[3] < b.SRC[3];
    }
};

/*
Create blade segments according to Zhao et al '09's equal annulus rule
*/
void creatBladSegm(std::vector<double> &r, const int segments)
{
	double *radius = new double[segments];

	radius[0]=1;
	radius[1]=sqrt(2);
	for(int i=2;i<segments;i++)
	{
		radius[i]=sqrt(2*radius[i-1]*radius[i-1]-radius[i-2]*radius[i-2]);
	}

	for(int i=0;i<segments;i++)
		radius[i]=radius[i]/radius[segments-1];

	r[0] = 0.5*radius[0];
	for(int i=1;i<segments;i++)
		r[i] = 0.5*(radius[i]+radius[i-1]);

	delete radius;
}


void initSource(Bodies &bodies)
{
	for( B_iter B=bodies.begin(); B!=bodies.end(); ++B )
	{
		B->IBODY = B-bodies.begin();
		B->SRC = 0;
		B->SRC[2] = 0.0 ;

		B->SRC[3] = 0.2;  //sigma (core size)

		B->X[0] = 0.0;
		B->X[1] = 0.0;
		B->X[2] = 0.0;
	}
}


void initTarget(Bodies &bodies, std::vector<double> x, std::vector<double> y)
{
	int numTarget = bodies.end() - bodies.begin();
	for( B_iter B=bodies.begin(); B!=bodies.end(); ++B )
	{
		B->IBODY = B-bodies.begin();
		B->IPROC = MPIRANK;

		int i = B-bodies.begin();

		B->TRG = 0;
		B->TRG[0] = 0;
		B->TRG[1] = 0;
		B->TRG[2] = 0;

		B->X[0] = x[i];
		B->X[1] = y[i];
		B->X[2] = 0.0;

//		std::cout << B->X[0] << "\t" << B->X[1] << std::endl;
	}
}


/*
Will give blade points at current time assuming it starts at theta = 0 at time = 0
Input -> dimensions, time
*/
void getBladePoints(std::vector<double> &x, std::vector<double> &y, double time, double rpm, double chord, double span, const int segments)
{
	std::vector<double> r(segments);
	creatBladSegm(r, segments); //Get co-ordinates of mid-point of blade in r direction

	double radPerSec = rpm*2*M_PI/(60.0);
	double theta = radPerSec*time;

	for(int i=0;i<segments;i++)
	{
		r[i] = r[i]*span;
		x[i] = r[i]*cos(theta);
		y[i] = r[i]*sin(theta);

//		std::cout << x[i] << "\t" << y[i] << "\t" << std::endl;
	}
}


void getBladeVel(std::vector<double> &solU, std::vector<double> &solV, std::vector<double> &solW, std::vector<double> x, std::vector<double> y, double time, double rpm, int segments)
{
	double radPerSec = rpm*2*M_PI/(60.0);
	double theta = radPerSec*time;

	std::vector<double> r(segments);

	for(int i=0;i<segments;i++)
	{
		r[i] = sqrt(pow(x[i],2)+pow(y[i],2));
		double rNormVel = r[i]*radPerSec;
		solU[i] = -rNormVel*sin(theta);
		solV[i] = rNormVel*cos(theta);
		solW[i] = 0;
	}
}


/*
Get bound circulation of a segment
*/
double getBoundCirculationNACA0015(double alpha, double vel, double chord)
{
	std::ifstream readFile;

	readFile.open("naca0015.txt");

	std::vector<double> alp, cl;

	double temp1, temp2, temp;
	while(!readFile.eof())
	{
		readFile >> temp1 >> temp2 >> temp >> temp >> temp >> temp >> temp;
		alp.push_back(temp1);
		cl.push_back(temp2);
	}

	alpha = alpha*180.0/M_PI;
	double secCl; //Cl of segment
	for(int i=0;i<alp.size()-1;i++)
	{
		if ((alpha >= alp[i]) && (alpha < alp[i+1]))
		{
//			std::cout << alpha << "\t" << alp[i] << std::endl;
			secCl = cl[i] + (alpha-alp[i])*(cl[i+1]-cl[i])/(alp[i+1]-alp[i]);
		}
	}
	readFile.close();

	double boundCir = 0.5*vel*chord*secCl;//0.5*rho*v*v*c*Cl = rho*v*Gamma
	return boundCir;
}

/*
Get bound circulation of a segment
*/
double getLiftCurveSlopeNACA0015()
{
	std::ifstream readFile;

	readFile.open("naca0015.txt");

	std::vector<double> alp, cl;

	double temp1, temp2, temp;
	while(!readFile.eof())
	{
		readFile >> temp1 >> temp2 >> temp >> temp >> temp >> temp >> temp;
		alp.push_back(temp1);
		cl.push_back(temp2);
	}

	double alpha = 0*180.0/M_PI; //Get slope at alpha=0
	double clAlpha; //Lift Curve Slope segment
	for(int i=0;i<alp.size()-1;i++) 
	{
		if ((alpha >= alp[i]) && (alpha < alp[i+1]))
		{
//			std::cout << alp[i] << "\t" << alp[i+1] << "\t" << cl[i] << "\t" << cl[i+1]<< std::endl;
			clAlpha = (cl[i+1]-cl[i])/(alp[i+1]-alp[i]);
		}
	}
	readFile.close();
	
	return clAlpha*180.0/M_PI; //Scale for radians
}


void getBoundCirculation(std::vector<double>  &boundCir, std::vector<double> alpha, std::vector<double> vel, std::vector<double> &x, std::vector<double> &y, const double &chord, const double &span)
{
//	for(int i=0;i<alpha.size();i++)
//	{
//		boundCir[i] = getBoundCirculationNACA0015(alpha[i], vel[i], chord);
//		std::cout << alpha[i] << "\t" << vel[i] << "\t" << boundCir[i] << std::endl;
//	}

//	double clAlpha = getLiftCurveSlopeNACA0015();
	double clAlpha = 0.2*180/M_PI;

	int segments = boundCir.size();

	std::vector<double> Acirc((segments-2)*(segments-2));
	std::vector<double> bCirc(segments-2);
	std::vector<double> solCirc(segments-2);

	std::vector<double> rSeg(segments);//Radial location of segments
	for(int i=0;i<segments;i++)
		rSeg[i] = sqrt(x[i]*x[i]+y[i]*y[i]);
	for(int i=0;i<segments-2;i++)
	{
		
		for(int j=0;j<segments-2;j++)
		{
			/**
			Use discrete vortex modeling approach to solve for vorticity
			*/
			int index = i*(segments-2)+j;
			double coeff1 = 0.5*clAlpha*chord*0.25*M_PI*(1/(rSeg[i+1]-0.5*(rSeg[j]+rSeg[j+1])));
			double coeff2 = -0.5*clAlpha*chord*0.25*M_PI*(1/(rSeg[i+1]-0.5*(rSeg[j+1]+rSeg[j+2])));
			Acirc[index] = coeff1+coeff2;
//			std::cout << Acirc[index] << "\t" ;
		}
//		std::cout << std::endl;
		int index = i*(segments-2)+i;
		Acirc[index] += 1;
		bCirc[i] = 0.5*vel[i+1]*alpha[i+1]*clAlpha*chord;
	}
	int columns = segments-2;

	solveBICG(Acirc, bCirc, columns, solCirc);

	for(int i=0;i<segments-2;i++)
		boundCir[i+1] = solCirc[i];

	boundCir[0] = 0.0;//Impose boundary condition at root and tip explicitly
	boundCir[segments-1] = 0.0;

//	std::ofstream wrFile;
//	std::string name="circ";
//	wrFile.open(name);
//	for(int i=0;i<segments;i++)
//		wrFile << rSeg[i] << "\t" << boundCir[i] << std::endl;	

//	wrFile.close();

}


double getThreePtDeri(double rMinus, double r, double rPlus, double fnMinus, double fn, double fnPlus)
{
	double eps = getEps();

	double delRPlusHalf = rPlus-r;
	double delRMinusHalf = r-rMinus;
	//Coefficients for 3 point derivative
	double aMin = -delRPlusHalf/(delRMinusHalf*(delRPlusHalf+delRMinusHalf)+eps);
	double a0 = (delRPlusHalf-delRMinusHalf)/((delRPlusHalf*delRMinusHalf)+eps);
	double aPlus = delRMinusHalf/(delRPlusHalf*(delRPlusHalf+delRMinusHalf)+eps);

	return aMin*fnMinus+a0*fn+aPlus*fnPlus;
}


//Do the derivative computation to get vorticity source from bound circulation
void getVorticitySource(std::vector<double> &shedVort, std::vector<double> &trailVort, std::vector<double>  &x, std::vector<double>  &y, std::vector<double> &circT0, std::vector<double> &circT1, double dt)
{
	int segments = circT0.size();

	for(int i=0;i<segments;i++)
	{
		shedVort[i] = -1*(circT1[i]-circT0[i])/dt;//-dGamma/dt

		double r = sqrt(pow(x[i],2)+pow(y[i],2));
		if((i>0) && (i<segments-1))
		{
			double rPlus = sqrt(pow(x[i+1],2)+pow(y[i+1],2));
			double rMinus  = sqrt(pow(x[i-1],2)+pow(y[i-1],2));

//Test the derivative
//			std::cout << i << "\t" << getThreePtDeri(i-1, i, i+1, pow(i-1,2), pow(i,2), pow(i+1,2)) << std::endl;

			trailVort[i] = getThreePtDeri(rMinus, r, rPlus, circT1[i-1], circT1[i], circT1[i+1]);
//			std::cout << rMinus << "\t" << trailVort[i] << "\t" << rPlus << std::endl;
//			std::cout << rMinus << "\t" << circT1[i-1] << "\t" << r << "\t" << circT1[i] << std::endl;
		}
		else if (i==0)
		{
			double rPlus = sqrt(pow(x[i+1],2)+pow(y[i+1],2));
			trailVort[i] =  (circT1[i+1]-circT1[i])/(rPlus-r);
		}
		else if (i==(segments-1))
		{
			double rMinus  = sqrt(pow(x[i-1],2)+pow(y[i-1],2));
			trailVort[i] =  (circT1[i]-circT1[i-1])/(r-rMinus);//divergence of gamma
		}
	}
}



void getSourceVector(std::vector<double> &sourceX, std::vector<double> &sourceY, std::vector<double> &sourceZ, std::vector<double> &shedVort, std::vector<double> &trailVort, std::vector<double> &u, std::vector<double> &v, std::vector<double> &w, double theta)
{
	int segments = shedVort.size();

	for(int i=0;i<segments;i++)
	{
		sourceX[i] = shedVort[i]*cos(theta)-u[i]*trailVort[i];//Vb is minus of v//
		sourceY[i] = shedVort[i]*sin(theta)-v[i]*trailVort[i];//Vb is minus of v
		sourceZ[i] = 			   -w[i]*trailVort[i];//Vb is minus of v

//		std::cout << sourceX[i] << "\t" << sourceY[i] << "\t" << sourceZ[i] << std::endl;
	}
}



/*
Assume that at the present time the blade got here from theta-dt*omega
*/
void getSegmentMid(std::vector<double> &ringMidX, std::vector<double> &ringMidY, std::vector<double> &x, std::vector<double> &y, double rpm, double t, double dt)
{

	int segments = x.size();

	double dThetaHalf = rpm*2*M_PI/60.0;
	double theta = t*rpm*2*M_PI/60.0;
	double ringMidTheta = theta-dThetaHalf;
	for(int i=0;i<segments;i++)
	{
		double r=sqrt(x[i]*x[i]+y[i]*y[i]);
		ringMidX[i] = r*cos(ringMidTheta);
		ringMidY[i] = r*sin(ringMidTheta);
	}

}


void accumSource(Bodies &bodies, std::vector<double> &ringMidX, std::vector<double> &ringMidY, std::vector<double> &sourceX, std::vector<double> &sourceY, std::vector<double> &sourceZ, double sigma, double h)
{
	int size = bodies.size();
	bodies.resize(size+sourceX.size());

	int segments = ringMidX.size();
	for(B_iter B=bodies.begin()+size;B!=bodies.end();++B)
	{
		int i = B-(bodies.begin()+size);
		B->ICELL = 0;

		B->SRC[0] = sourceX[i]*h*h*h;
		B->SRC[1] = sourceY[i]*h*h*h;
		B->SRC[2] = sourceZ[i]*h*h*h;

		B->SRC[3] = sigma;  //sigma (core size)

		B->X[0] = ringMidX[i];
		B->X[1] = ringMidY[i];
		B->X[2] = 0.0;

		B->TRG[0] = 0.0;
		B->TRG[1] = 0.0;
		B->TRG[2] = 0.0;
		B->TRG[3] = 0.0;
	}

//	for( B_iter B=bodies.begin(); B!=bodies.end(); ++B )
//	{
//		std::cout << B->SRC << std::endl;
//	}
}


//Update according to Vortex Transport Equation
void update(Bodies &bodies, std::vector<double> &dxdt, std::vector<double> &dydt, std::vector<double> &dzdt, double dt, double nu)
{
	for(B_iter B=bodies.begin();B!=bodies.end();++B)
	{
		int i = B-bodies.begin();		

		B->X[0] += dxdt[i] * dt; //Update positions from Biot-Savart velocities
		B->X[1] += dydt[i] * dt;
		B->X[2] += dzdt[i] * dt;

		B->SRC[0] += B->TRG[0] * dt; //Source values change according to stretching
		B->SRC[1] += B->TRG[1] * dt;
		B->SRC[2] += B->TRG[2] * dt;

		B->SRC[3] += nu / B->SRC[3] * dt; //Diffusion 
	}
}


int main() {
  IMAGES = 0;
  THETA = 1 / sqrtf(4);
  Bodies bodies, jbodies, kbodies;
  Cells cells, jcells;

	std::string kernelName = "BiotSavart";

	Dataset D;
	TreeConstructor T;

	T.setKernel(kernelName);
	T.initialize();

	D.kernelName = kernelName;
//Major FMM parameters are in types.h

	const double dt = 5e-4   ;
	const double nu = 1e-2;
	/*
	Define blade characteristics
	*/
	const double rpm = 960; const double chord = 2*0.0254; const double span = 30*0.0254;
	double alpha = 4*M_PI/180.0;
	const int segments = 50;
	std::vector<double> x(segments), y(segments);

	double timePerRev = 60.0/rpm;
	int stepsForOneRev = timePerRev/dt;

	double h = 0.025*span;//Resolution of vorticity (Seems arbitrary)
	double sigma = 1.3*h;//Vorticity Core size from Zhao '09

	int numBodies = 0;
	int numTarget = segments;
	bodies.resize(numBodies);
	initSource(bodies);

	std::vector<double> boundCirX(segments), boundCirY(segments);

  for(int timeIt=0;timeIt<5000;timeIt++)
  {
	double t = timeIt*dt;

	std::cout << "Physical time elapsed\t" << t << std::endl;

	getBladePoints(x, y, t, rpm, chord, span, segments);//x, y has the blade points

	T.setKernel("BiotSavart");

	jbodies.resize(numTarget);
	initTarget(jbodies, x, y);
//	T.evalP2P(jbodies, bodies);//To check the FMM results

	kbodies = jbodies;
	jcells.clear();
	T.setDomain(kbodies);

	for( B_iter B=kbodies.begin(); B!=kbodies.end(); ++B )
	{
		int i = B-kbodies.begin();
		B->SRC[3] = i;//bottomup sorts the bodies. This will help me get the correct indices 
	}


	T.bottomup(kbodies,jcells);//Targets have to be converted to cells for FMM
				//This step sorts the bodies, has to be unsorted to use
	D.initTarget(kbodies);
	T.evalP2M(jcells);
	T.evalM2M(jcells);

	if(timeIt > 0)
		T.downward(jcells,cells,1);//I am using the cells created below from previous time step

	std::vector<double> solU(segments), solV(segments), solW(segments); //Solid Blade Velocity
	getBladeVel(solU, solV, solW, x, y, t, rpm, segments);

	std::vector<double> indU(segments), indV(segments), indW(segments); //Induced Velocity from vortices
	for( B_iter B=kbodies.begin(); B!=kbodies.end(); ++B )
	{
		indU[B->SRC[3]] = B->TRG[0];
		indV[B->SRC[3]] = B->TRG[1];
		indW[B->SRC[3]] = B->TRG[2];
//		if (timeIt > 5)
//			std::cout << indU[i] << "\t" << indV[i] << "\t" << indW[i] << std::endl;
//			std::cout << B->TRG << "\t" << kbodies[segments-i-1].TRG<< std::endl;
	}


	double theta = t*(rpm*2*M_PI/60.0);
	double thetaVel;


	std::vector<double> u(segments), v(segments), w(segments); //net velocity and alpha
	std::vector<double> netVel(segments), netAlpha(segments); //net velocity and alpha
	for(int i=0;i<segments;i++)
	{
		u[i] = solU[i] - indU[i]; //Is this RIGHT?
		v[i] = solV[i] - indV[i]; //Whole part needs rechecking for logic
		w[i] = solW[i] - indW[i];

		double totXYVel = sqrt(pow(u[i],2)+pow(v[i],2));//Velocity in XY plane
		thetaVel = atan2(v[i], u[i]);//Theta of velocity
		double normXYDirection = atan2(cos(theta), -sin(theta));

		double normXYVel = totXYVel*cos(thetaVel-normXYDirection);
		double flowDirection = atan2(w[i], normXYVel);
		netAlpha[i] = alpha - flowDirection;
		netVel[i] = sqrt(pow(normXYVel,2)+pow(w[i],2));
//		std::cout << theta << "\t" << thetaVel << "\t" << normXYVel[i] << std::endl;
	}

	std::vector<double> boundCir(segments);
//	getBoundCirculation(boundCir, netAlpha, netVel, chord, x, y, span);//convert to vector
	getBoundCirculation(boundCir, netAlpha, netVel, x, y, chord, span);//convert to vector

	std::vector<double> temp(segments) ;
//	std::vector<double> tempX(segments), tempY(segments);
	for(int i=0;i<segments;i++)
	{
		temp[i] = boundCir[i];//To do time differentiation
//		tempY[i] = boundCirY[i];
//		boundCirX[i] = boundCir[i]*cos(theta);
//		boundCirY[i] = boundCir[i]*sin(theta);
//		std::cout << t << "\t" << tempX[i] << "\t" << boundCirX[i] << std::endl;
	}

	std::vector<double> shedVort(segments), trailVort(segments);
	getVorticitySource(shedVort, trailVort, x, y, temp, boundCir, dt);

	std::vector<double> sourceX(segments), sourceY(segments), sourceZ(segments);
	getSourceVector(sourceX, sourceY, sourceZ, shedVort, trailVort, u, v, w, theta) ;

	std::vector<double> ringMidX(segments), ringMidY(segments);
	getSegmentMid(ringMidX, ringMidY, x, y, rpm, t, dt);//Vorticity sources are to be placed at mid point of trajectory

	accumSource(bodies, ringMidX, ringMidY, sourceX, sourceY, sourceZ, sigma, h);
//	std::cout << bodies.size() << std::endl;

	T.setDomain(bodies);
	cells.clear(); //NEVER FORGET this step. Creates all sorts of errors
	T.bottomup(bodies,cells);
	jcells = cells;
	T.downward(cells,jcells,1);

	/*
	1. BiotSavart
	2. Stretching
	3. update

	Bottomup is done only once. Bottomup includes P2M and M2M. So for each kernel I have to do it again.
	*/
	
	D.initTarget(bodies);//NEVER Forget this step. The values are incremented. So if the base value is wrong, so is the final value
	jcells = cells;
	T.downward(cells,jcells,1);

	//BiotSavart
	T.setKernel("BiotSavart");
	D.initTarget(bodies);
	T.evalP2M(cells);//So everytime bottomup is done, this is evaluated
	T.evalM2M(cells);
	jcells = cells;
	T.downward(cells,jcells,1);

//	T.buffer = bodies; //TEST FMM values against P2P values
//	D.initTarget(T.buffer);
//	T.evalP2P(T.buffer,bodies);
	
//	for(B_iter B=bodies.begin();B!=bodies.end();++B)
//	{
//		int i = B-bodies.begin();
//		std::cout << B->TRG << T.buffer[i].TRG <<std::endl;
//	}


	int numSourc = bodies.size();
	std::vector<double> dxdt(numSourc), dydt(numSourc), dzdt(numSourc);

	for(B_iter B=bodies.begin();B!=bodies.end();++B)
	{
		int i=B-bodies.begin();
		dxdt[i] = B->TRG[0];
		dydt[i] = B->TRG[1];
		dzdt[i] = B->TRG[2];

//		std::cout << dxdt[i] << "\t" << dydt[i] << "\t" << dzdt[i] << std::endl;
	}


	//Stretching
	T.setKernel("Stretching");
	D.initTarget(bodies);
	T.evalP2M(cells);
	T.evalM2M(cells);
	jcells = cells;
	T.downward(cells,jcells,1);

	//Update source and position
	update(bodies, dxdt, dydt, dzdt, dt, nu);

//	T.buffer = bodies;
//	D.initTarget(T.buffer);
//	T.evalP2P(T.buffer,bodies);
//	for(B_iter B=bodies.begin();B!=bodies.end();++B)
//	{
//		int i = B-bodies.begin();
//		std::cout << B->X   <<std::endl;
//	}


	if (((timeIt%(stepsForOneRev/4)) == 0) && (timeIt > 1))
	{

		double radiusGrid = 2*span; int radiusPoints = 50;
		int thetaPoints = 50;
		double zMin = -2*span;double zMax = 0.5*span; int zPoints = 100;
		int totalPoints = radiusPoints*thetaPoints*zPoints;
		int indexCtr;

		Bodies gridBodies;
		Cells gridCells, gridJCells;

		gridBodies.resize(totalPoints);

		for(int i1=0;i1<zPoints;i1++)
		{
			double zPoint = zMin + i1*(zMax - zMin)/double(zPoints-1);
			for(int i2=0;i2<thetaPoints;i2++)
			{
				double thetaPoint = 0 + i2*(2*M_PI)/double(thetaPoints-1); 
				for(int i3=0;i3<radiusPoints;i3++)
				{
					double rPoint = 0 + i3*radiusGrid/double(radiusPoints-1);
					double x = rPoint*cos(thetaPoint);
					double y = rPoint*sin(thetaPoint);
					double z = zPoint;
					indexCtr = i1*thetaPoints*radiusPoints+i2*radiusPoints+i3;
					gridBodies[indexCtr].X[0] = x;
					gridBodies[indexCtr].X[1] = y;
					gridBodies[indexCtr].X[2] = z;
					gridBodies[indexCtr].ICELL = 0;

					gridBodies[indexCtr].SRC[3] = indexCtr;	//gridBodies are sorted to do FMM. This will ensure I write in order
										//I will use this to sort them back
				}
			}
		}


		gridCells.clear();
		T.setDomain(gridBodies); //setDomain is a global function of the kernel. The grid domain is bigger than particle domain
					//Therefore it has to be reset
		T.bottomup(gridBodies, gridCells);
		T.setKernel("Gaussian");
		D.initTarget(gridBodies);
		T.evalP2M(gridCells);
		T.evalM2M(gridCells);
		T.downward(gridCells,cells,1);//Get TRG[0]

		std::vector<double> gridWriteVortX(gridBodies.size());
		for(B_iter B=gridBodies.begin();B!=gridBodies.end();++B)//Write x comp of vorticity	
		{
	//		std::cout << B->SRC[3] << std::endl;
			gridWriteVortX[B->SRC[3]] = B->TRG[0];//Use the index to easily write the values at the correct place		
		}

		Bodies tempBodies = bodies;
		for(B_iter B=tempBodies.begin();B!=tempBodies.end();++B)
		{
			int i = B-tempBodies.begin();
			B->SRC[0] = bodies[i].SRC[1];
		}
		Cells tempCells1, tempCells2;
		T.setDomain(tempBodies);
		tempCells1.clear(); //NEVER FORGET this step. Creates all sorts of errors
		T.bottomup(tempBodies,tempCells1);
		tempCells2=tempCells1;
		T.downward(tempCells1, tempCells2, 1);

		gridCells.clear();
		T.setDomain(gridBodies); //setDomain is a global function of the kernel. The grid domain is bigger than particle domain
					//Therefore it has to be reset
		T.bottomup(gridBodies, gridCells);
		T.setKernel("Gaussian");
		D.initTarget(gridBodies);
		T.evalP2M(gridCells);
		T.evalM2M(gridCells);
		T.downward(gridCells,tempCells1,1);//Get TRG[0]

		std::vector<double> gridWriteVortY(gridBodies.size());
		for(B_iter B=gridBodies.begin();B!=gridBodies.end();++B)//Write x comp of vorticity	
			gridWriteVortY[B->SRC[3]] = B->TRG[0];//Use the index to easily write the values at the correct place		

		for(B_iter B=tempBodies.begin();B!=tempBodies.end();++B)
		{
			int i = B-tempBodies.begin();
			B->SRC[0] = bodies[i].SRC[2];
		}
		T.setDomain(tempBodies);
		tempCells1.clear(); //NEVER FORGET this step. Creates all sorts of errors
		T.bottomup(tempBodies,tempCells1);
		tempCells2=tempCells1;
		T.downward(tempCells1, tempCells2, 1);

		gridCells.clear();
		T.setDomain(gridBodies); //setDomain is a global function of the kernel. The grid domain is bigger than particle domain
					//Therefore it has to be reset
		T.bottomup(gridBodies, gridCells);
		T.setKernel("Gaussian");
		D.initTarget(gridBodies);
		T.evalP2M(gridCells);
		T.evalM2M(gridCells);
		T.downward(gridCells,tempCells1,1);//Get TRG[0]

		std::vector<double> gridWriteVortZ(gridBodies.size());
		for(B_iter B=gridBodies.begin();B!=gridBodies.end();++B)//Write x comp of vorticity	
			gridWriteVortZ[B->SRC[3]] = B->TRG[0];//Use the index to easily write the values at the correct place		

		
		std::sort(gridBodies.begin(), gridBodies.end(), ordering());
		
		
		std::ofstream wrFile;
		int noOfRev = timeIt/(stepsForOneRev/4);
		std::string name="vtkOut"+std::to_string(noOfRev)+".vtk";
		wrFile.open(name);
		wrFile << "# vtk DataFile Version 2.0 \nVortex Data \nASCII" << std::endl << "DATASET STRUCTURED_GRID" << std::endl;
		wrFile << "DIMENSIONS \t"<< radiusPoints << "\t" << thetaPoints <<  "\t" << zPoints <<  std::endl;
		wrFile << "POINTS\t" << radiusPoints*thetaPoints*zPoints << "\tfloat" << std::endl;
		for( B_iter B=gridBodies.begin(); B!=gridBodies.end(); ++B )
		{
	//		if (B->X[0] < 0)
	//			std::cout << B->X[0] << std::endl;
			wrFile << B->X[0] << "\t" << B->X[1] << "\t" << B->X[2] <<  std::endl;
		}
		wrFile << std::endl << "POINT_DATA\t" << radiusPoints*thetaPoints*zPoints <<  std::endl;
		wrFile << "VECTORS vorticity float " << std::endl ;

		for(int i=0;i<gridWriteVortX.size();i++)
		{
			wrFile << gridWriteVortX[i]  << "\t" << gridWriteVortY[i] << "\t" << gridWriteVortZ[i] <<  std::endl;
		}

		wrFile.close();
	}//If loop

	}//For loop


	T.finalize();


}
