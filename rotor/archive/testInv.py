import numpy as np
import scipy.sparse.linalg as spla

class LiftingLine:


	def __init__(self):
		self.chord = 2*0.0254
		self.span = 30*0.0254
		self.rpm = 960
		self.tipSpeed = 75
		self.alpha = 6*np.pi/180.0
		self.clAlpha = 0.2*180.0/np.pi
		
		self.tol = 1e-7

		return


	def runLiftLine(self):
		segments = 50

		A = np.zeros((segments, segments), float)
		b = np.zeros(segments, float)

		rdFile = open('mat.inv', 'r')
		rdFile1 = open('vort.inv', 'r')
		coun = 0
		for i in rdFile:
			counj=0
			for j in i.split():
				A[coun,counj] = float(j)
				counj+=1
			b[coun] = float(rdFile1.readline())
			coun += 1

		print A

#		print spla.lgmres(A, b)
		print spla.bicgstab(A[2:segments-2, 2:segments-2], b[2:segments-2])



if __name__=="__main__":
	run = LiftingLine()
	run.runLiftLine()
