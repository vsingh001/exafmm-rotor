#include "hermite.cpp"

double getEps()
{
	return std::numeric_limits<double>::epsilon();
}


struct ordering {
    bool operator ()(Body const& a, Body const& b) {
        return a.SRC[3] < b.SRC[3];
    }
};


/*
Create blade segments according to Zhao et al '09's equal annulus rule
*/
void creatBladSegm(std::vector<double> &r, const int segments)
{
	double *radius = new double[segments];

	radius[0]=1;
	radius[1]=sqrt(2);
	for(int i=2;i<segments;i++)
	{
		radius[i]=sqrt(2*radius[i-1]*radius[i-1]-radius[i-2]*radius[i-2]);
	}

	for(int i=0;i<segments;i++)
		radius[i]=radius[i]/radius[segments-1];

	r[0] = 0.5*radius[0];
	for(int i=1;i<segments;i++)
		r[i] = 0.5*(radius[i]+radius[i-1]);

	delete radius;
}


/**
Get (r_2)^2-(r_1)^2. Since it is by equal annulus rule, it will be just one value
*/
void getNonDimAnnulusR0(double &dr, const int segments)
{
	std::vector<double> r(segments);
	creatBladSegm(r, segments);
	dr = pow(2*r[0],2); 
//	std::cout << r[0] << "\t" << dr << std::endl;
}


/*
Will give blade points at current time assuming it starts at theta = 0 at time = 0
Input -> dimensions, time
*/
void getBladePoints(std::vector<double> &x, std::vector<double> &y, double time, double rpm, double chord, double span, const int segments, const double &dt)
{
	std::vector<double> r(segments);
	creatBladSegm(r, segments); //Get co-ordinates of mid-point of blade in r direction

	double radPerSec = rpm*2*M_PI/(60.0);
//	double theta = radPerSec*(time+0.5*dt);
	double theta = radPerSec*(time);

	for(int i=0;i<segments;i++)
	{
		r[i] = r[i]*span;
		x[i] = r[i]*cos(theta);
		y[i] = r[i]*sin(theta);

//		std::cout << x[i] << "\t" << y[i] << "\t" << std::endl;
	}
}


void getBladeVel(std::vector<double> &solU, std::vector<double> &solV, std::vector<double> &solW, std::vector<double> x, std::vector<double> y, double time, double rpm, int segments)
{
	double radPerSec = rpm*2*M_PI/(60.0);
	double theta = radPerSec*time;

	std::vector<double> r(segments);

	for(int i=0;i<segments;i++)
	{
		r[i] = sqrt(pow(x[i],2)+pow(y[i],2));
		double rNormVel = r[i]*radPerSec;
		solU[i] = -rNormVel*sin(theta);
		solV[i] = rNormVel*cos(theta);
		solW[i] = 0;
	}
}



void getBoundCirculation(std::vector<double>  &boundCir, std::vector<double> alpha, std::vector<double> vel, std::vector<double> &x, std::vector<double> &y, const double &chord, const double &span)
{
//	for(int i=0;i<alpha.size();i++)
//	{
//		boundCir[i] = getBoundCirculationNACA0015(alpha[i], vel[i], chord);
//		std::cout << alpha[i] << "\t" << vel[i] << "\t" << boundCir[i] << std::endl;
//	}

//	double clAlpha = getLiftCurveSlopeNACA0015();
	double clAlpha = 0.2*180/M_PI;

	int segments = boundCir.size();

	std::vector<double> Acirc((segments-2)*(segments-2));
	std::vector<double> bCirc(segments-2);
	std::vector<double> solCirc(segments-2);

	std::vector<double> rSeg(segments);//Radial location of segments
	for(int i=0;i<segments;i++)
		rSeg[i] = sqrt(x[i]*x[i]+y[i]*y[i]);
	for(int i=0;i<segments-2;i++)
	{
		
		for(int j=0;j<segments-2;j++)
		{
			/**
			Use discrete vortex modeling approach to solve for vorticity
			*/
			int index = i*(segments-2)+j;
			double coeff1 = 0.5*clAlpha*chord*0.25*M_PI*(1/(rSeg[i+1]-0.5*(rSeg[j]+rSeg[j+1])));
			double coeff2 = -0.5*clAlpha*chord*0.25*M_PI*(1/(rSeg[i+1]-0.5*(rSeg[j+1]+rSeg[j+2])));
			Acirc[index] = coeff1+coeff2;
//			std::cout << Acirc[index] << "\t" ;
		}
//		std::cout << std::endl;
		int index = i*(segments-2)+i;
		Acirc[index] += 1;
		if (alpha[i+1]*180.0/M_PI > 15)//Limiting to stall
			alpha[i+1] = 15*M_PI/180.0;
		if (alpha[i+1]*180/M_PI <-10)
			alpha[i+1] =-10*M_PI/180.0;

		bCirc[i] = 0.5*vel[i+1]*alpha[i+1]*clAlpha*chord;
		std::cout << "Segment wise velocity and alpha\t" << vel[i+1] << "\t" << alpha[i+1]*180.0/M_PI << std::endl; 	
	}
	
	double accum = 0;
	for(int i=1; i<segments-2 ;i++)
		accum += bCirc[i]*(rSeg[i]-rSeg[i-1])	;
	std::cout << "Circulation is \t" << accum << std::endl; 	

	int columns = segments-2;

	solveBICG(Acirc, bCirc, columns, solCirc);

	for(int i=0;i<segments-2;i++)
		boundCir[i+1] = solCirc[i];

	boundCir[0] = 0.0;//Impose boundary condition at root and tip explicitly
	boundCir[segments-1] = 0.0;

	
	for(int i=0; i<segments; i++)
	{
//		std::cout << vel[i] << "\t" << alpha[i] << std::endl;
//		std::cout << vel[i] << "\t" << boundCir[i] << std::endl;
	}

//	std::ofstream wrFile;
//	std::string name="circ";
//	wrFile.open(name);
//	for(int i=0;i<segments;i++)
//		wrFile << rSeg[i] << "\t" << boundCir[i] << std::endl;	
//
//	wrFile.close();

}


double getThreePtDeri(double rMinus, double r, double rPlus, double fnMinus, double fn, double fnPlus)
{
	double eps = getEps();

	double delRPlusHalf = rPlus-r;
	double delRMinusHalf = r-rMinus;
	//Coefficients for 3 point derivative
	double aMin = -delRPlusHalf/(delRMinusHalf*(delRPlusHalf+delRMinusHalf)+eps);
	double a0 = (delRPlusHalf-delRMinusHalf)/((delRPlusHalf*delRMinusHalf)+eps);
	double aPlus = delRMinusHalf/(delRPlusHalf*(delRPlusHalf+delRMinusHalf)+eps);

	return aMin*fnMinus+a0*fn+aPlus*fnPlus;
}


//Do the derivative computation to get vorticity source from bound circulation
void getVorticitySource(std::vector<double> &shedVort, std::vector<double> &trailVort, std::vector<double>  &x, std::vector<double>  &y, std::vector<double> &circT0, std::vector<double> &circT1, double dt)
{
	int segments = circT0.size();

	double maxVort=0;

	double integGamma = 0;
	double sigmaCirc = 0;
	double integCirc  = 0;

	vector<double> rSeg(segments);
	for(int i=0;i<segments;i++)
	{
		shedVort[i] = -1*(circT1[i]-circT0[i])/dt;//-dGamma/dt

		double r = sqrt(pow(x[i],2)+pow(y[i],2));
		rSeg[i] = r;
		if((i>0) && (i<segments-1))
		{
			double rPlus = sqrt(pow(x[i+1],2)+pow(y[i+1],2));
			double rMinus  = sqrt(pow(x[i-1],2)+pow(y[i-1],2));

//Test the derivative
//			std::cout << i << "\t" << getThreePtDeri(i-1, i, i+1, pow(i-1,2), pow(i,2), pow(i+1,2)) << std::endl;

			trailVort[i] = getThreePtDeri(rMinus, r, rPlus, circT1[i-1], circT1[i], circT1[i+1]);
//			std::cout << rMinus << "\t" << trailVort[i] << "\t" << rPlus << std::endl;
//			std::cout << rMinus << "\t" << circT1[i-1] << "\t" << r << "\t" << circT1[i] << std::endl;
		}
		else if (i==0)
		{
			double rPlus = sqrt(pow(x[i+1],2)+pow(y[i+1],2));
			trailVort[i] =  (circT1[i+1]-circT1[i])/(rPlus-r);
		}
		else if (i==(segments-1))
		{
			double rMinus  = sqrt(pow(x[i-1],2)+pow(y[i-1],2));
			trailVort[i] =  (circT1[i]-circT1[i-1])/(r-rMinus);//divergence of gamma
		}

//		std::cout << r << "\t" << trailVort[i] << std::endl;
		if (i>0)
		{
			integGamma += trailVort[i]*(rSeg[i]-rSeg[i-1]);
			sigmaCirc += circT1[i];
			integCirc += circT1[i]*(rSeg[i]-rSeg[i-1]);
		}

		maxVort = fmax(abs(trailVort[i]), maxVort);
	}

//	double scaleFactor = abs(integCirc/integGamma);
//	for(int i=0;i<segments;i++)
//		trailVort[i] = trailVort[i]*scaleFactor;

//	std::cout << "Max trail vort calculated is \t" << maxVort << std::endl;
//	std::cout << "Integral of gamma and circulation is \t" << integGamma  << "\t" <<  integCirc << std::endl;
}



void getSourceVector(std::vector<double> &sourceX, std::vector<double> &sourceY, std::vector<double> &sourceZ, std::vector<double> &shedVort, std::vector<double> &trailVort, std::vector<double> &u, std::vector<double> &v, std::vector<double> &w, double theta)
{
	int segments = shedVort.size();

	for(int i=0;i<segments;i++)
	{
		sourceX[i] =0;  
		sourceY[i] =0; 
		sourceZ[i] =0;
	}

	for(int i=1; i<segments; i++)//Vorticity at root doing weird things
	{
//		sourceX[i] = shedVort[i]*cos(theta)-u[i]*trailVort[i];//Vb is minus of v//
//		sourceY[i] = shedVort[i]*sin(theta)-v[i]*trailVort[i];//Vb is minus of v
//		sourceZ[i] = 			   -w[i]*trailVort[i];//Vb is minus of v
		sourceX[i] = -u[i]*trailVort[i];//Vb is minus of v//
		sourceY[i] = -v[i]*trailVort[i];//Vb is minus of v
		sourceZ[i] = -w[i]*trailVort[i];//Vb is minus of v
		//shed vorticity calculation is a problem, we'll come back to it later


//		std::cout << sourceX[i] << "\t" << sourceY[i] << "\t" << sourceZ[i] << std::endl;
	}
}



/*
Assume that at the present time the blade got here from theta-dt*omega
*/
void getSegmentMid(std::vector<double> &ringMidX, std::vector<double> &ringMidY, std::vector<double> &x, std::vector<double> &y, double rpm, double t, double dt)
{

	int segments = x.size();

	double dThetaHalf = 0.5*dt*rpm*2*M_PI/60.0;
	double theta = t*rpm*2*M_PI/60.0;
	double ringMidTheta = theta-dThetaHalf;
	for(int i=0;i<segments;i++)
	{
		double r=sqrt(x[i]*x[i]+y[i]*y[i]);
		ringMidX[i] = r*cos(ringMidTheta);
		ringMidY[i] = r*sin(ringMidTheta);
	}

}


void createGrid(std::vector<double> &inX, std::vector<double> &inY, std::vector<double> &outX, std::vector<double> &outY, const double &span)
{
		int segments = outX.size();
		double theta = atan2(inY[segments-1], inX[segments-1]);

		for(int i=0;i<segments;i++)
		{
			outX[i] = (i+1)*span*(cos(theta))/segments;
			outY[i] = (i+1)*span*(sin(theta))/segments;
		}
}


/**Create a new grid, take sources and use gaussian+bicg to determine source values
Pass it to accumSource to add as sources
llX -> lifting line X
lvX -> lifting line vorticity gamma
alphaX -> vorticity*volume
*/
void reMesh(std::vector<double> &llX, std::vector<double> &llY, std::vector<double> &lvX, std::vector<double> &lvY, std::vector<double> &lvZ, double &sigma, const double &span, double &annulusArea, std::vector<double> &cellX, std::vector<double> &cellY, std::vector<double> &alphX, std::vector<double> &alphY, std::vector<double> &alphZ)
{
	int segments = llX.size();

	sigma = sigma;

//	std::cout << "reMesh\t" << annulusArea << std::endl;
//	for(int i=0;i<segments;i++)
//	{
//		std::cout << i << "\t"<< annulusArea*lvX[i] << "\t" << annulusArea*lvY[i] << std::endl;
//	}
	std::vector<double> AAlpha((segments)*(segments));
	std::vector<double> bAlphaX(segments),bAlphaY(segments),bAlphaZ(segments);
	std::vector<double> solAlphaX(segments), solAlphaY(segments), solAlphaZ(segments);

	Bodies sourceBodies, sinkBodies;

	sourceBodies.resize(1);
	sinkBodies.resize(segments);

	for(int i=0;i<segments;i++)
	{
		sinkBodies[i].X[0]=llX[i];
		sinkBodies[0].X[1]=llY[i];
		sinkBodies[0].X[2]=0.0;

		sinkBodies[0].SRC = 0.0;
		sinkBodies[0].TRG = 0.0;
	}

	TreeConstructor T;
	Dataset D;

	std::string kernelName = "Gaussian";
	T.setKernel(kernelName);
        T.initialize();
		
        D.kernelName = kernelName;
	double accumX = 0;double accumY = 0;double accumZ = 0;
	for(int i=0;i<segments;i++)
	{
		sourceBodies[0].X[0]=llX[i];
		sourceBodies[0].X[1]=llY[i];
		sourceBodies[0].X[2]=0.0;

		sourceBodies[0].SRC = 0.0;
		sourceBodies[0].SRC[0] = 1.0;
		sourceBodies[0].SRC[3] = sigma;
		sourceBodies[0].TRG = 0.0;

		D.initTarget(sinkBodies);
		T.evalP2P(sinkBodies, sourceBodies);
		for(int j=0;j<segments;j++)
		{
			int index = j*segments+i;
			AAlpha[index] = sinkBodies[j].TRG[0];
//			std::cout << AAlpha[index] << "\t";
		}
		bAlphaX[i]=annulusArea*lvX[i];
		bAlphaY[i]=annulusArea*(lvY[i+1]-lvY[i]);
		bAlphaZ[i]=annulusArea*lvZ[i];
		accumX+=bAlphaX[i];
		accumY+=bAlphaY[i];
		accumZ+=bAlphaZ[i];
//		std::cout << std::endl;
	}
	bAlphaY[segments-1]=bAlphaY[segments-2];

	if (accumX<getEps())
	{
		for(int i=0;i<segments;i++)
			solAlphaX[i]	=0.0;
	}
	else
		solveBICG(AAlpha, bAlphaX, segments, solAlphaX);

	if (accumY<getEps())
	{
		for(int i=0;i<segments;i++)
			solAlphaY[i]	=0.0;
	}
	else
		solveBICG(AAlpha, bAlphaY, segments, solAlphaY);
	if (accumZ<getEps())
	{
		for(int i=0;i<segments;i++)
			solAlphaZ[i]	=0.0;
	}
	else
		solveBICG(AAlpha, bAlphaZ, segments, solAlphaZ);


	for(int i=0;i<segments;i++)
	{
	std::cout << bAlphaY[i] << "\t" << solAlphaY[i] << std::endl;
	}

	createGrid(llX, llY, cellX, cellY, 1.20*span);

	sourceBodies.resize(segments);

	for(int i=0;i<segments;i++)
	{
		sourceBodies[i].X[0]=llX[i];
		sourceBodies[i].X[1]=llY[i];
		sourceBodies[i].X[2]=0.0;

		sourceBodies[i].SRC = 0.0;
		sourceBodies[i].SRC[0] = solAlphaY[i];
		sourceBodies[i].SRC[3] = sigma;
		sourceBodies[i].TRG = 0.0;

		sinkBodies[i].X[0]=cellX[i];
		sinkBodies[i].X[1]=cellY[i];
		sinkBodies[i].X[2]=0.0;

		sinkBodies[i].SRC = 0.0;
		sinkBodies[i].TRG = 0.0;
	}

	T.evalP2P(sinkBodies, sourceBodies);

	for(int i=0;i<segments;i++)
	{
//		std::cout << "\t" << bAlphaY[i] << "\t" << sinkBodies[i].TRG[0]<<std::endl;
	}
}

/**
The Gaussian in ExaFMM operates using only SRC[0] and TRG[0]
This function will just take SourceBodies, do Gaussian for all dimensions and return sinkBodies
*/
void multiGauss(Bodies &sourceBodies, Bodies &sinkBodies)
{
	TreeConstructor T;
	Dataset D;

	std::string kernelName = "Gaussian";
	T.setKernel(kernelName);
        T.initialize();
		
        D.kernelName = kernelName;

	Bodies tempSourceBodies = sourceBodies;
	Bodies tempSinkBodies = sinkBodies;

	D.initTarget(tempSinkBodies);
	T.evalP2P(tempSinkBodies, tempSourceBodies);

	for(B_iter B=tempSinkBodies.begin(); B!=tempSinkBodies.end();++B)
	{
		int i = B-tempSinkBodies.begin();
		sinkBodies[i].TRG[0] = B->TRG[0];
	}

	for(B_iter B=tempSourceBodies.begin(); B!=tempSourceBodies.end();++B)
	{
		int i = B-tempSourceBodies.begin();
		B->SRC[0] = sourceBodies[i].SRC[1];
	}

	tempSinkBodies = sinkBodies;
	D.initTarget(tempSinkBodies);
	T.evalP2P(tempSinkBodies, tempSourceBodies);

	for(B_iter B=tempSinkBodies.begin(); B!=tempSinkBodies.end();++B)
	{
		int i = B-tempSinkBodies.begin();
		sinkBodies[i].TRG[1] = B->TRG[0];
	}

	for(B_iter B=tempSourceBodies.begin(); B!=tempSourceBodies.end();++B)
	{
		int i = B-tempSourceBodies.begin();
		B->SRC[0] = sourceBodies[i].SRC[2];
	}

	D.initTarget(tempSinkBodies);
	T.evalP2P(tempSinkBodies, tempSourceBodies);

	for(B_iter B=tempSinkBodies.begin(); B!=tempSinkBodies.end();++B)
	{
		int i = B-tempSinkBodies.begin();
		sinkBodies[i].TRG[2] = B->TRG[0];
	}
}


/**
Create a grid consisting of 3 rows of particles with segments number of columns
The span is scaled by 1.20 because significant vorticity is seen till that location
*/
void createSourceGrid(std::vector<double> &inX, std::vector<double> &inY, const double &span, std::vector<double> &outX,std::vector<double> &outY, std::vector<double> &outZ, double &volume) 
{
	int segments = inX.size();
	double theta = atan2(inY[segments-1], inX[segments-1]);
	
	double gridScale = 1.20;//Spanwise scaling
	assert(gridScale > 1);

	std::vector<double> rSeg(segments); 
	for(int i=0;i<segments;i++)
	{
		rSeg[i] = sqrt(pow(inX[i],2)+pow(inY[i],2));
	}
	double h = rSeg[1]-rSeg[0]; 
	
	int xtraCells = (gridScale-1)*span/h;
//	std::cout << xtraCells << "\t" << h << std::endl;
	outX.resize(segments+xtraCells);outY.resize(segments+xtraCells);outZ.resize(segments+xtraCells);

	for(int i=0;i<segments;i++)
	{
		outX[i] = rSeg[i]*cos(theta);
		outY[i] = rSeg[i]*sin(theta);
		outZ[i] = 0.0; 
	}

	for(int i=0;i<xtraCells;i++)
	{
		outX[segments+i] = (rSeg[segments-1]+h*(i+1))*cos(theta);
		outY[segments+i] =  (rSeg[segments-1]+h*(i+1))*sin(theta);
		outZ[segments+i] = 0.0; 
	}

	volume = h*h*h;
//	std::cout << outX[segments-1]-outX[segments-2] << std::endl;
}




/**
Do piecewise linear interpolation to get circulation values at the regular grid.
Create a new source grid. Diffuse vorticity into them using particle strength exchange
llX -> lifting line X
lvX -> lifting line vorticity gamma
alphaX -> vorticity*volume
*/
void reMesh2(std::vector<double> &llX, std::vector<double> &llY, std::vector<double> &lvX, std::vector<double> &lvY, std::vector<double> &lvZ, double &sigma, const double &span, double &annulusArea, std::vector<double> &cellX, std::vector<double> &cellY, std::vector<double> &alphX, std::vector<double> &alphY, std::vector<double> &alphZ, const double &dt)
{
	int segments = llX.size();

	std::vector<double> outX(segments), outY(segments);
	createGrid(llX, llY, outX, outY, span);

	std::vector<double> rSeg(segments), rGrid(segments);
	for(int i=0;i<segments;i++)
	{
		rSeg[i] = sqrt(pow(llX[i],2) + pow(llY[i],2));
		rGrid[i] = sqrt(pow(outX[i],2) + pow(outY[i],2));
//		std::cout << rSeg[i] << "\t" << rGrid[i]<< std::endl;
	}

	std::vector<double> gridVX(segments), gridVY(segments); 
	for(int i=0;i<segments;i++)
	{
		if(rGrid[i]<rSeg[0])
		{
			gridVX[i] = lvX[0] + (rGrid[i]-rSeg[0])*(lvX[1]-lvX[0])/(rSeg[1]-rSeg[0]);	
			gridVY[i] = lvY[0] + (rGrid[i]-rSeg[0])*(lvY[1]-lvY[0])/(rSeg[1]-rSeg[0]);	
		}
		if(rGrid[i]>rSeg[segments-1])
		{
			gridVX[i] = lvX[segments-1] + (rGrid[i]-rSeg[segments-1])*(lvX[segments-1]-lvX[segments-2])/(rSeg[segments-1]-rSeg[segments-2]);		
			gridVY[i] = lvY[segments-1] + (rGrid[i]-rSeg[segments-1])*(lvY[segments-1]-lvY[segments-2])/(rSeg[segments-1]-rSeg[segments-2]);		
		}
		for(int j=0;j<segments;j++)
		{
			if ((rGrid[i]>=rSeg[j]) && (rGrid[i]<rSeg[j+1]))
			{
				gridVX[i] = lvX[j] + (rGrid[i]-rSeg[j])*(lvX[j+1]-lvX[j])/(rSeg[j+1]-rSeg[j]);	
				gridVY[i] = lvY[j] + (rGrid[i]-rSeg[j])*(lvY[j+1]-lvY[j])/(rSeg[j+1]-rSeg[j]);	
			}
		}

		gridVY[i] = gridVY[i]*annulusArea;//Converting circulation to vorticity 
		gridVX[i] = gridVX[i]*annulusArea;//Ideally it should be lvX = lvX*annulusArea 
						//But since the interpolation is linear, it works
//		std::cout << llX[i] << "\t" << outX[i] << "\t" << gridVY[i] << "\t" << lvY[i]<< std::endl;
//		std::cout << gridVX[i]/gridVY[i] << "\t" << gridVY[i] << std::endl; 
	}

	std::vector<double> sourceX, sourceY, sourceZ; 
	double cellVol;

	createSourceGrid(outX, outY, span, sourceX, sourceY, sourceZ, cellVol);
	int noGridCells = sourceX.size();

	cellX.resize(noGridCells);
	cellY.resize(noGridCells);
	for(int i=0;i<noGridCells;i++)
	{
		cellX[i] = sourceX[i];	
		cellY[i] = sourceY[i];	
	}

	std::vector<double> sourceVX(noGridCells),sourceVY(noGridCells),sourceVZ(noGridCells);

	for(int i=0;i<noGridCells;i++)
	{
		sourceVX[i] = 0.0;
		sourceVY[i] = 0.0;
		sourceVZ[i] = 0.0;
	}
	for(int i=0;i<segments;i++)
	{
		sourceVX[i] = gridVX[i]*cellVol;//Converting to alpha
		sourceVY[i] = gridVY[i]*cellVol;
	}


	Bodies sourceBodies, sinkBodies;
	sourceBodies.resize(noGridCells); sinkBodies.resize(noGridCells);

	for(int i=0;i<noGridCells;i++)
	{
		sourceBodies[i].X[0]=sourceX[i];
		sourceBodies[i].X[1]=sourceY[i];
		sourceBodies[i].X[2]=sourceZ[i];

		sourceBodies[i].SRC =  0.0;
		sourceBodies[i].SRC[0] = sourceVX[i];
		sourceBodies[i].SRC[1] = sourceVY[i];

		sourceBodies[i].SRC[3] = sigma;
		sourceBodies[i].TRG = 0.0;
//		std::cout << sourceVX[i]/sourceVY[i] << "\t" << sourceVY[i] << std::endl;
	}

	sinkBodies = sourceBodies;
	multiGauss(sourceBodies, sinkBodies);

	double absVortX0, absVortX1;
	double absVortY0, absVortY1;
	double absVortZ0, absVortZ1;
	absVortX0=0; absVortX1=0;
	absVortY0=0; absVortY1=0;
	absVortZ0=0; absVortZ1=0;

	for(int i=0;i<noGridCells;i++)
	{
		absVortX0 += abs(sourceVX[i]/cellVol);
		absVortX1 += abs(sinkBodies[i].TRG[0]);
		absVortY0 += abs(sourceVY[i]/cellVol);
		absVortY1 += abs(sinkBodies[i].TRG[1]);
		absVortZ0 += abs(sourceVZ[i]/cellVol);
		absVortZ1 += abs(sinkBodies[i].TRG[2]);
	}

//	std::cout << absVortY1 << "\t" << absVortY0 << std::endl;
	double scaleFactorX = absVortX0/(absVortX1+getEps());//Scale source values so that they have same total voriticity
						// as the lifting line

	double scaleFactorY = absVortY0/(absVortY1+getEps());
	double scaleFactorZ = absVortZ0/(absVortZ1+getEps());

	alphX.resize(noGridCells);
	alphY.resize(noGridCells);
	alphZ.resize(noGridCells);
	double sigmaAlpha = 0;
	for(int i=0;i<noGridCells;i++)
	{
		alphX[i] = sinkBodies[i].TRG[0]*scaleFactorX;
		alphY[i] = sinkBodies[i].TRG[1]*scaleFactorY;
		alphZ[i] = sinkBodies[i].TRG[2]*scaleFactorZ;
		double temp = sqrt(pow(alphX[i],2) + pow(alphY[i],2) + pow(alphZ[i],2));
		sigmaAlpha += temp;

//		std::cout << alphX[i]/alphY[i] << "\t" << alphY[i] << std::endl;
//		std::cout << alphY[i] <<"\t"<< sourceVY[i]/cellVol << std::endl;
//		if (sourceX[i] > 0.6)
//			std::cout << sourceX[i] << "\t" << alphY[i] << std::endl;
	}

	double scaleFactor = 1; 
//	double scaleFactor = 0.001/sigmaAlpha;
//	std::cout << "Sigma Alpha and scaleFactor is \t" << sigmaAlpha << "\t" << scaleFactor << std::endl;
	for(int i=0;i<noGridCells;i++)
	{
		alphX[i] = alphX[i]*scaleFactor;
		alphY[i] = alphY[i]*scaleFactor;
		alphZ[i] = alphZ[i]*scaleFactor;
	}
//	std::cout << sinkBodies[34].TRG[1]*scaleFactorY << "\t" << alphY[34] <<"\t"<< std::endl;

}




/**
Do piecewise linear interpolation to get circulation values at the regular grid.
Create a new source grid. Diffuse vorticity into them using particle strength exchange
llX -> lifting line X
lvX -> lifting line vorticity gamma
alphaX -> vorticity*volume
*/
void reMesh1(std::vector<double> &llX, std::vector<double> &llY, std::vector<double> &lvX, std::vector<double> &lvY, std::vector<double> &lvZ, double &sigma, const double &span, double &annulusArea, std::vector<double> &cellX, std::vector<double> &cellY, std::vector<double> &alphX, std::vector<double> &alphY, std::vector<double> &alphZ, const double &dt)
{
	int segments = llX.size();

	std::vector<double> outX(segments), outY(segments);
	createGrid(llX, llY, outX, outY, span);

	std::vector<double> rSeg(segments), rGrid(segments);
	for(int i=0;i<segments;i++)
	{
		rSeg[i] = sqrt(pow(llX[i],2) + pow(llY[i],2));
		rGrid[i] = sqrt(pow(outX[i],2) + pow(outY[i],2));
//		std::cout << rSeg[i] << "\t" << rGrid[i]<< std::endl;
	}

	std::vector<double> gridVX(segments), gridVY(segments); 
	for(int i=0;i<segments;i++)
	{
		if(rGrid[i]<rSeg[0])
		{
			gridVX[i] = lvX[0] + (rGrid[i]-rSeg[0])*(lvX[1]-lvX[0])/(rSeg[1]-rSeg[0]);	
			gridVY[i] = lvY[0] + (rGrid[i]-rSeg[0])*(lvY[1]-lvY[0])/(rSeg[1]-rSeg[0]);	
		}
		if(rGrid[i]>rSeg[segments-1])
		{
			gridVX[i] = lvX[segments-1] + (rGrid[i]-rSeg[segments-1])*(lvX[segments-1]-lvX[segments-2])/(rSeg[segments-1]-rSeg[segments-2]);		
			gridVY[i] = lvY[segments-1] + (rGrid[i]-rSeg[segments-1])*(lvY[segments-1]-lvY[segments-2])/(rSeg[segments-1]-rSeg[segments-2]);		
		}
		for(int j=0;j<segments;j++)
		{
			if ((rGrid[i]>=rSeg[j]) && (rGrid[i]<rSeg[j+1]))
			{
				gridVX[i] = lvX[j] + (rGrid[i]-rSeg[j])*(lvX[j+1]-lvX[j])/(rSeg[j+1]-rSeg[j]);	
				gridVY[i] = lvY[j] + (rGrid[i]-rSeg[j])*(lvY[j+1]-lvY[j])/(rSeg[j+1]-rSeg[j]);	
			}
		}

		gridVY[i] = gridVY[i]*annulusArea;//Converting circulation to vorticity 
		gridVX[i] = gridVX[i]*annulusArea;//Ideally it should be lvX = lvX*annulusArea 
						//But since the interpolation is linear, it works
//		std::cout << llX[i] << "\t" << outX[i] << "\t" << gridVY[i] << "\t" << lvY[i]<< std::endl;
//		std::cout << gridVX[i]/gridVY[i] << "\t" << gridVY[i] << std::endl; 
	}

	cellX = outX;
	cellY = outY;

	alphX.resize(segments);
	alphY.resize(segments);
	alphZ.resize(segments);
	for(int i=0;i<segments;i++)
	{
		alphX[i] = gridVX[i]; 
		alphY[i] = gridVY[i]; 
		alphZ[i] = 0.0; 
	}

}




