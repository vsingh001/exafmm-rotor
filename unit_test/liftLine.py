import numpy as np
import scipy.sparse.linalg as spla

class LiftingLine:


	def __init__(self):
		self.chord = 2*0.0254
		self.span = 30*0.0254
		self.rpm = 960
		self.tipSpeed = 75
		self.alpha = 6*np.pi/180.0
		self.clAlpha = 0.2*180.0/np.pi
		
		self.tol = 1e-7

		return


	def runLiftLine(self):
		segments = 100

		A = np.zeros((segments-2, segments-2), float)
		b = np.zeros(segments-2, float)

		v = np.linspace(0.01,self.tipSpeed,segments)
		r = np.linspace(0,self.span,segments)
#		dr = r[1]-r[0]	
#		r = np.linspace(dr,self.span-dr,segments)

		for i in range(segments-2):
			coeff1 = (0.5*self.clAlpha*self.chord)*(1/(4*np.pi))*(1/(r[i+1]-0.5*(r[1:segments-1]+r[0:segments-2])))
			coeff2 = -(0.5*self.clAlpha*self.chord)*(1/(4*np.pi))*(1/(r[i+1]-0.5*(r[1:segments-1]+r[2:segments])))
			A[i,:] = (coeff1+coeff2)
			A[i,i] = 1+A[i,i]
#			A[i,i] = 1

#		print A

		b = 0.5*v[1:segments-1]*self.clAlpha*self.alpha*self.chord

		print spla.bicgstab(A, b)



if __name__=="__main__":
	run = LiftingLine()
	run.runLiftLine()
