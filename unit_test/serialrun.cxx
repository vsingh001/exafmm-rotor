#include "dataset.h"
#include "construct.h"
#ifdef VTK
#include "vtk.h"
#endif

int main() {
  const int numBodies = 10000;
  std::string kernelName = "BiotSavart";
  IMAGES = 0;
  THETA = 1/sqrtf(3);
  Bodies bodies(numBodies);
  Bodies jbodies;
  Cells cells,jcells;
  Dataset D;
  TreeConstructor T;
  T.setKernel(kernelName);
  T.initialize();
  D.kernelName = kernelName;
  T.printNow = true;

  T.startTimer("Set bodies   ");
  D.random(bodies,1,1);
  T.stopTimer("Set bodies   ",T.printNow);
  T.eraseTimer("Set bodies   ");

  T.startTimer("Set domain   ");
  T.setDomain(bodies);
  T.stopTimer("Set domain   ",T.printNow);
  T.eraseTimer("Set domain   ");

#ifdef TOPDOWN
  T.topdown(bodies,cells);
#else
  T.bottomup(bodies,cells);
#endif
  jcells = cells;
  T.startTimer("Downward     ");
  T.downward(cells,jcells,1);
  T.stopTimer("Downward     ",T.printNow);
  T.eraseTimer("Downward     ");

  if( IMAGES != 0 ) {
    T.startTimer("Set periodic ");
    jbodies = T.periodicBodies(bodies);
    T.stopTimer("Set periodic ",T.printNow);
    T.eraseTimer("Set periodic ");
  } else {
    jbodies = bodies;
  }

  T.startTimer("Direct sum   ");
  T.buffer = bodies;
  D.initTarget(T.buffer);
  T.evalP2P(T.buffer,jbodies);
  T.stopTimer("Direct sum   ",T.printNow);
  T.eraseTimer("Direct sum   ");
  T.writeTime();
  T.writeTime();

	for( B_iter B=bodies.begin(); B!=bodies.end(); ++B ) 
	{
		int i = B-bodies.begin();
		std::cout << B->TRG << "\t" << T.buffer[i].TRG << std::endl;
	}


  real diff1 = 0, norm1 = 0, diff2 = 0, norm2 = 0;
  D.evalError(bodies,T.buffer,diff1,norm1,diff2,norm2);
  D.printError(diff1,norm1,diff2,norm2);
#ifdef VTK
  int Ncell = 0;
  vtkPlot vtk;
  vtk.setDomain(T.getR0(),T.getX0());
  vtk.setGroupOfPoints(bodies,Ncell);
  vtk.plot(Ncell);
#endif
  T.finalize();
}
